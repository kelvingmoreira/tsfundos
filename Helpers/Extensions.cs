﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections.ObjectModel;
using System.Reflection;
using System.ComponentModel;

namespace TSFundos.Helpers
{
    public static class Extensions
    {
        public static string GetEnumDescription(this Enum enumObj)
        {
            FieldInfo fieldInfo = enumObj.GetType().GetField(enumObj.ToString());

            object[] attribArray = fieldInfo.GetCustomAttributes(false);

            if (attribArray.Length == 0)
            {
                return enumObj.ToString();
            }
            else
            {
                DescriptionAttribute attrib = attribArray[0] as DescriptionAttribute;
                return attrib.Description;
            }
        }

        public static IEnumerable<string> ToCsv<T>(this ObservableCollection<T> list)
        {
            var properties = typeof(T).GetProperties();
            yield return string.Join(";", properties.Select(x => x.Name));

            foreach (var @object in list)
            {
                yield return string.Join(";", properties.Select(p => (p.GetValue(@object, null) ?? string.Empty).ToString()).ToArray());
            }
        }

        public static IEnumerable<string> ToCsv<T>(this ObservableCollection<T> list, params string[] properties)
        {
            yield return string.Join(";", properties);

            foreach (var @object in list)
            {
                yield return string.Join(";", properties.Select(p => (@object.GetType().GetProperty(p).GetValue(@object, null) ?? string.Empty).ToString()).ToArray());
            }
        }

        public static string ReplaceInvalidChars(this string filename)
        {
            return string.Join("", filename.Split(Path.GetInvalidFileNameChars()));
        }
    }
}
