﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Gestora.Fundos;
using Gestora.Fundos.Passivo;
using Gestora.Primitivos;
using Gestora.Dados;
using Gestora.Dados.Fundos;
using Gestora.Dados.Fundos.Passivo;

namespace GestoraTests
{
    [TestClass]
    public class FundosTests
    {
        [TestMethod]
        public void GerarFundos()
        {
            CotistasProvider cotistas = new CotistasProvider();
            cotistas.RunSearch();
            BoletasProvider boletas = new BoletasProvider();
            boletas.Cotistas = cotistas.Collection;
            boletas.RunSearch();
        }
    }
}
