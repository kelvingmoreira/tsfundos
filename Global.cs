﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TSFundos
{
    public sealed class Global
    {
        static Global _instancia;
        public static Global Instancia
        {
            get { return _instancia ?? (_instancia = new Global()); }

        }
        private Global() { }
        
        public bool FezLogin { get; set; }
        public string Nome { get; set; }
        public string ID { get; set; }
    }
}
