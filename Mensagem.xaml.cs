﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace TSFundos
{
    /// <summary>
    /// Interação lógica para Mensagem.xam
    /// </summary>
    public partial class Mensagem : UserControl, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string nomePropriedade)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nomePropriedade));
        }

        private string _Mensagem;
        public string Texto { get { return _Mensagem; } set { _Mensagem = value; OnPropertyChanged("Texto"); } }

        public Mensagem(string CorpodaMensagem)
        {
            InitializeComponent();
            Texto = CorpodaMensagem;
            DataContext = this;
        }

        private async void BtOK_Click(object sender, RoutedEventArgs e)
        {
            var dialog = (sender as DependencyObject).TryFindParent<BaseMetroDialog>();
            MetroWindow mw = Application.Current.MainWindow as MetroWindow;
            await mw.HideMetroDialogAsync(dialog);
        }
    }
}
