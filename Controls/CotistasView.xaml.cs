﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace TSFundos.Controls
{
    /// <summary>
    /// Interação lógica para CotistasView.xam
    /// </summary>
    public partial class CotistasView : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #endregion

        #region ItemsSource DependencyProperty

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(CotistasView), new PropertyMetadata(null, new PropertyChangedCallback(OnItemsSourcePropertyChanged)));

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)this.GetValue(ItemsSourceProperty); }
            set { this.SetValue(ItemsSourceProperty, value); }
        }

        private static void OnItemsSourcePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {

        }

        #endregion

        #region Detalhes Command

        public static readonly DependencyProperty DetalhesCommandProperty = DependencyProperty.Register("DetalhesCommand", typeof(ICommand), typeof(UserControl));

        public ICommand DetalhesCommand
        {
            get { return (ICommand)GetValue(DetalhesCommandProperty); }
            set { SetValue(DetalhesCommandProperty, value); }
        }

        #endregion

        #region Alterar Command

        public static readonly DependencyProperty AlterarCommandProperty = DependencyProperty.Register("AlterarCommand", typeof(ICommand), typeof(UserControl));

        public ICommand AlterarCommand
        {
            get { return (ICommand)GetValue(AlterarCommandProperty); }
            set { SetValue(AlterarCommandProperty, value); }
        }

        #endregion

        #region Remover Command

        public static readonly DependencyProperty RemoverCommandProperty = DependencyProperty.Register("RemoverCommand", typeof(ICommand), typeof(UserControl));

        public ICommand RemoverCommand
        {
            get { return (ICommand)GetValue(RemoverCommandProperty); }
            set { SetValue(RemoverCommandProperty, value); }
        }

        #endregion

        public CotistasView()
        {
            InitializeComponent();
        }
    }
}
