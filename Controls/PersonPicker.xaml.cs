﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace TSFundos.Controls
{
    /// <summary>
    /// Interação lógica para PersonPicker.xam
    /// </summary>
    public partial class PersonPicker : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #region Evento ClickSearch

        public static readonly RoutedEvent ClickSearchEvent = EventManager.RegisterRoutedEvent("ClickSearch", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PersonPicker));

        public event RoutedEventHandler ClickSearch
        {
            add { AddHandler(ClickSearchEvent, value); }
            remove { RemoveHandler(ClickSearchEvent, value); }
        }

        void RaiseClickEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(PersonPicker.ClickSearchEvent);
            RaiseEvent(newEventArgs);
        }

        void OnClick()
        {
            RaiseClickEvent();
        }

        #endregion

        #region ItemsSource DependencyProperty

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable), typeof(PersonPicker), new PropertyMetadata(null, new PropertyChangedCallback(OnItemsSourcePropertyChanged)));

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)this.GetValue(ItemsSourceProperty); }
            set { this.SetValue(ItemsSourceProperty, value); }
        }

        private static void OnItemsSourcePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            /*var control = sender as PersonPicker;
            if (control != null)
                control.OnItemsSourceChanged((IEnumerable)e.OldValue, (IEnumerable)e.NewValue);*/

            PersonPicker thiscontrol = sender as PersonPicker;

            if (e.OldValue != e.NewValue) thiscontrol.ItemsSource = (IEnumerable)e.NewValue;
        }

        /*private void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        {
            // Remove handler for oldValue.CollectionChanged
            var oldValueINotifyCollectionChanged = oldValue as INotifyCollectionChanged;

            if (null != oldValueINotifyCollectionChanged)
            {
                oldValueINotifyCollectionChanged.CollectionChanged -= new NotifyCollectionChangedEventHandler(newValueINotifyCollectionChanged_CollectionChanged);
            }
            // Add handler for newValue.CollectionChanged (if possible)
            var newValueINotifyCollectionChanged = newValue as INotifyCollectionChanged;
            if (null != newValueINotifyCollectionChanged)
            {
                newValueINotifyCollectionChanged.CollectionChanged += new NotifyCollectionChangedEventHandler(newValueINotifyCollectionChanged_CollectionChanged);
            }

        }

        void newValueINotifyCollectionChanged_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //Do your stuff here.
        }*/

        #endregion

        #region DisplayMemberPath DependencyProperty

        public static readonly DependencyProperty DisplayMemberPathProperty = DependencyProperty.Register("DisplayMemberPath", typeof(string), typeof(PersonPicker), null);

        public string DisplayMemberPath
        {
            get { return (string)this.GetValue(DisplayMemberPathProperty); }
            set { this.SetValue(DisplayMemberPathProperty, value); }
        }

        #endregion

        #region SelectedItem DependencyProperty

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register("SelectedItem", typeof(object), typeof(PersonPicker), 
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, new PropertyChangedCallback(OnSelectedItemPropertyChanged)));

        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        private static void OnSelectedItemPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            //PersonPicker thiscontrol = sender as PersonPicker;

            //if (e.OldValue != e.NewValue) thiscontrol.SelectedItem = e.NewValue;
        }

        #endregion

        #region Watermark DependencyProperty

        public static readonly DependencyProperty WatermarkProperty = DependencyProperty.Register("Watermark", typeof(string), typeof(PersonPicker),
            new FrameworkPropertyMetadata(null, new PropertyChangedCallback(WatermarkPropertyChanged)));

        public string Watermark
        {
            get { return (string)GetValue(WatermarkProperty); }
            set { SetValue(WatermarkProperty, value); }
        }

        private static void WatermarkPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            //PersonPicker thiscontrol = sender as PersonPicker;

            //if (e.OldValue != e.NewValue) thiscontrol.SelectedItem = e.NewValue;
        }

        #endregion

        private string resultado;
        public string Resultado { get { return resultado; } set { resultado = value; OnPropertyChanged(); } }

        public PersonPicker()
        {
            InitializeComponent();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            OnClick();
            TxPerson.Visibility = Visibility.Collapsed;
            CbQuery.Visibility = Visibility.Visible;
            CbQuery.Focus();
            if (CbQuery.Items.Count != 0) CbQuery.IsDropDownOpen = true;
        }

        private void TxPerson_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return || e.Key == Key.Enter)
            {
                OnClick();
                TxPerson.Visibility = Visibility.Collapsed;
                CbQuery.Visibility = Visibility.Visible;
                CbQuery.Focus();
                if (CbQuery.Items.Count != 0) CbQuery.IsDropDownOpen = true;
            }
        }

        private void CbQuery_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Back || e.Key == Key.Delete)
            {
                TxPerson.Visibility = Visibility.Visible;
                CbQuery.Visibility = Visibility.Collapsed;
                CbQuery.SelectedValue = null;
                CbQuery.IsDropDownOpen = false;
                e.Handled = true;
                TxPerson.Focus();
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            TxPerson.Visibility = Visibility.Visible;
            CbQuery.Visibility = Visibility.Collapsed;
            CbQuery.IsDropDownOpen = false;
            CbQuery.SelectedValue = null;
            TxPerson.Focus();
        }

        private void ComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (!(sender as ComboBox).IsEditable && (e.Key == Key.Enter || e.Key == Key.Return || e.Key == Key.Space))
                if (!(sender as ComboBox).IsDropDownOpen) (sender as ComboBox).IsDropDownOpen = true;
        }
    }
}
