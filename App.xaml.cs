﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Input;
using MahApps.Metro.Controls;
using TSFundos.Dialogs;

namespace TSFundos
{
    /// <summary>
    /// Interação lógica para App.xaml
    /// </summary>
    public partial class App : Application
    {
        RadialGradientBrush BorderColor = new RadialGradientBrush();
        RadialGradientBrush ButtonBorderColor = new RadialGradientBrush();
        RadialGradientBrush ContentAreaColor = new RadialGradientBrush();
        RadialGradientBrush ContentAreaColor2 = new RadialGradientBrush();

        public App()
        {
            InitializeComponent();
            ResourceDictionary resource = App.Current.Resources;
            //Highlight
            BorderColor.GradientStops.Add(new GradientStop((Color)Resources["BrilhoColor"], 0.0));
            //Lowlight
            BorderColor.GradientStops.Add(new GradientStop((Color)Resources["DestaqueColor"], 0.8));
            BorderColor.RadiusY = 2.1;

            //Highlight
            ButtonBorderColor.GradientStops.Add(new GradientStop(Color.FromRgb(38, 151, 177), 0.0));
            //Lowlight
            ButtonBorderColor.GradientStops.Add(new GradientStop(Color.FromRgb(38, 70, 97), 0.5));
            ButtonBorderColor.RadiusY = 2.1;
            ButtonBorderColor.RadiusX = 1;

            //Highlight
            ContentAreaColor.GradientStops.Add(new GradientStop(Color.FromArgb(32, 38, 151, 177), 0.0));
            //Lowlight
            ContentAreaColor.GradientStops.Add(new GradientStop(Color.FromArgb(0, 255, 255, 255), 0.28));
            ContentAreaColor.RadiusY = 10;
            ContentAreaColor.RadiusX = 3;

            //Highlight
            ContentAreaColor2.GradientStops.Add(new GradientStop(Color.FromArgb(26, 38, 151, 177), 0.0));
            //Lowlight
            ContentAreaColor2.GradientStops.Add(new GradientStop(Color.FromArgb(0, 255, 255, 255), 0.28));
            ContentAreaColor2.RadiusY = 8;
            ContentAreaColor2.RadiusX = 2;
        }

        private void Border_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Border border = sender as Border;
            Point pt = Mouse.GetPosition(border);
            BorderColor.GradientOrigin = new Point(pt.X / border.ActualWidth, pt.Y / border.ActualHeight);
            BorderColor.Center = BorderColor.GradientOrigin;
            border.BorderBrush = BorderColor;
        }

        private void Button_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Border border = sender as Border;
            Point pt = Mouse.GetPosition(border);
            ButtonBorderColor.GradientOrigin = new Point(pt.X / border.ActualWidth, pt.Y / border.ActualHeight);
            ButtonBorderColor.Center = ButtonBorderColor.GradientOrigin;
            ContentAreaColor2.GradientOrigin = new Point(pt.X / border.ActualWidth, pt.Y / border.ActualHeight);
            ContentAreaColor2.Center = ButtonBorderColor.GradientOrigin;
            border.BorderBrush = ButtonBorderColor;
            border.Background = ContentAreaColor2;
        }

        private void Border_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Border border = sender as Border;
            border.BorderBrush = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
            BorderColor.GradientOrigin = new Point(0.5, 0.5);   // Default
            BorderColor.Center = BorderColor.GradientOrigin;
        }

        private void Button_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Border border = sender as Border;
            border.BorderBrush = new SolidColorBrush(Color.FromArgb(0, 22, 29, 49));
            border.Background = Brushes.Transparent;
            ButtonBorderColor.GradientOrigin = new Point(0.5, 0.5);   // Default
            ButtonBorderColor.Center = ButtonBorderColor.GradientOrigin;
        }

        private void FluentBig_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Border border = sender as Border;
            Point pt = Mouse.GetPosition(border);
            ButtonBorderColor.GradientOrigin = new Point(pt.X / border.ActualWidth, pt.Y / border.ActualHeight);
            ButtonBorderColor.Center = ButtonBorderColor.GradientOrigin;
            ContentAreaColor.GradientOrigin = new Point(pt.X / border.ActualWidth, pt.Y / border.ActualHeight);
            ContentAreaColor.Center = ButtonBorderColor.GradientOrigin;
            border.BorderBrush = ButtonBorderColor;
            border.Background = ContentAreaColor;
        }

        private void FluentBig_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Border border = sender as Border;
            border.BorderBrush = new SolidColorBrush(Color.FromArgb(0, 22, 29, 49));
            border.Background = Brushes.Transparent;
            ButtonBorderColor.GradientOrigin = new Point(0.5, 0.5);   // Default
            ButtonBorderColor.Center = ButtonBorderColor.GradientOrigin;
        }

        private void TextBox_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            ((TextBox)sender).SelectAll();
            ((TextBox)sender).BringIntoView();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mw = Current.MainWindow as MainWindow;
            mw.FrConteúdo.GoBack();
        }

        private void ComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (!(sender as ComboBox).IsEditable && (e.Key == Key.Enter || e.Key == Key.Return || e.Key == Key.Space))
                if(!(sender as ComboBox).IsDropDownOpen) (sender as ComboBox).IsDropDownOpen = true;
            if (TextBoxHelper.GetClearTextButton((sender as ComboBox)) && (e.Key == Key.Back || e.Key == Key.Delete))
            {
                (sender as ComboBox).SelectedValue = null;
                e.Handled = true;
            }
        }

        private async void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            DialogManager dialogManager = new DialogManager();
            await dialogManager.ShowMessageBox("Erro", "Um erro inesperado ocorreu: " + e.Exception.Message);
            e.Handled = true;
        }
    }
}
