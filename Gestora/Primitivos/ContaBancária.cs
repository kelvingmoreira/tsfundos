﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Gestora.Dados;

namespace Gestora.Primitivos
{
    [SqlTableMapper(TableName = "ContasBancárias", PrimaryKey = nameof(ID))]
    public class ContaBancária : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged members
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        #region Campos

        private int id;
        private string nome;
        private string banco;
        private string agência;
        private string conta;
        private string dígito;

        #endregion

        #region Propriedades

        [SqlMapper(ColumnName = nameof(ID))]
        public int ID { get { return id; } set { id = value; OnPropertyChanged(); } }
        [SqlMapper(ColumnName = nameof(Nome))]
        public string Nome { get { return nome; } set { nome = value; OnPropertyChanged(); } }
        [SqlMapper(ColumnName = nameof(Banco))]
        public string Banco { get { return banco; } set { banco = value; OnPropertyChanged(); } }
        [SqlMapper(ColumnName = nameof(Agência))]
        public string Agência { get { return agência; } set { agência = value; OnPropertyChanged(); } }
        [SqlMapper(ColumnName = nameof(Conta))]
        public string Conta { get { return conta; } set { conta = value; OnPropertyChanged(); } }
        [SqlMapper(ColumnName = nameof(Dígito))]
        public string Dígito { get { return dígito; } set { dígito = value; OnPropertyChanged(); } }

        #endregion
    }
}
