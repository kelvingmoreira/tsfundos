﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gestora.Dados;

namespace Gestora.Primitivos
{
    public class Administrador
    {
        [SqlMapper(ColumnName = nameof(Código))]
        public string Código { get; set; }

        [SqlMapper(ColumnName = nameof(RazãoSocial))]
        public string RazãoSocial { get; set; }
    }
}
