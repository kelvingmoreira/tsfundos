﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestora
{
    public sealed class Global
    {
        static Global _instancia;
        public static Global Instancia
        {
            get { return _instancia ?? (_instancia = new Global()); }

        }
        private Global() { }

        public string GestoraCnpj { get; set; } = "13.194.316/0001-03";
    }
}
