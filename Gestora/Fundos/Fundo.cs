﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Gestora.Dados;
using Gestora.Primitivos;
using Gestora.Fundos.Passivo;
using System.Collections.ObjectModel;

namespace Gestora.Fundos
{
    [SqlTableMapper(TableName = "Fundos", PrimaryKey = nameof(Código))]
    public class Fundo : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #region Campos

        private string código;
        private string razãosocial;
        private string nomefantasia;
        private Administrador administrador;
        private Custodiante custodiante;
        private double? aplicaçãomínima;
        private double? aplicaçãomínimainicial;
        private double? resgatemínimo;
        private double? saldomínimo;
        private int conversãoaplicação;
        private int conversãoresgate;
        private int liquidaçãoaplicação;
        private int liquidaçãoresgate;
        private ObservableCollection<Distribuidor> distribuidores;

        #endregion

        [SqlMapper(ColumnName = nameof(Código))]
        public string Código { get { return código; } set { código = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(RazãoSocial))]
        public string RazãoSocial { get { return razãosocial; } set { razãosocial = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(NomeFantasia))]
        public string NomeFantasia { get { return nomefantasia; } set { nomefantasia = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(Administrador))]
        public Administrador Administrador { get { return administrador; } set { administrador = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(Custodiante))]
        public Custodiante Custodiante { get { return custodiante; } set { custodiante = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(AplicaçãoMínima))]
        public double? AplicaçãoMínima { get { return aplicaçãomínima; } set { aplicaçãomínima = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(AplicaçãoMínimaInicial))]
        public double? AplicaçãoMínimaInicial { get { return aplicaçãomínimainicial; } set { aplicaçãomínimainicial = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(ResgateMínimo))]
        public double? ResgateMínimo { get { return resgatemínimo; } set { resgatemínimo = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(SaldoMínimo))]
        public double? SaldoMínimo { get { return saldomínimo; } set { saldomínimo = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(ConversãoAplicação))]
        public int ConversãoAplicação { get { return conversãoaplicação; } set { conversãoaplicação = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(ConversãoResgate))]
        public int ConversãoResgate { get { return conversãoresgate; } set { conversãoresgate = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(LiquidaçãoAplicação))]
        public int LiquidaçãoAplicação { get { return liquidaçãoaplicação; } set { liquidaçãoaplicação = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(LiquidaçãoResgate))]
        public int LiquidaçãoResgate { get { return liquidaçãoresgate; } set { liquidaçãoresgate = value; OnPropertyChanged(); } }

        public ObservableCollection<Distribuidor> Distribuidores { get { return distribuidores; } set { distribuidores = value; OnPropertyChanged(); } }

        public override string ToString()
        {
            return NomeFantasia;
        }
    }
}
