﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestora.Fundos.Passivo
{
    public class Transferência
    {
        public Cotista De { get; set; }
        public Cotista Para { get; set; }
        public DateTime DataTransferência { get; set; }
    }
}
