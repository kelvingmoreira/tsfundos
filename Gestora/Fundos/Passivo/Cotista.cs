﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Gestora.Dados;
using Gestora.Primitivos;

namespace Gestora.Fundos.Passivo
{
    [SqlTableMapper(TableName = "Cotista", PrimaryKey = nameof(ID))]
    public class Cotista : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        [SqlMapper(ColumnName = nameof(ID))]
        public int ID { get; set; }
        [SqlMapper(ColumnName = nameof(Nome))]
        public string Nome { get; set; }
        [SqlMapper(ColumnName = nameof(NomeIdentificador))]
        public string NomeIdentificador { get; set; }
        [SqlMapper(ColumnName = nameof(TipoPessoa))]
        public TipoPessoa TipoPessoa { get; set; }
        [SqlMapper(ColumnName = nameof(Cpf))]
        public string Cpf { get; set; }
        [SqlMapper(ColumnName = nameof(Cnpj))]
        public string Cnpj { get; set; }
        [SqlMapper(ColumnName = nameof(Distribuidor))]
        public Distribuidor Distribuidor { get; set; }
        [SqlMapper(ColumnName = nameof(Transferência))]
        public Transferência Transferência { get; set; }
        [SqlMapper(ColumnName = nameof(ContaBancária))]
        public ContaBancária ContaBancária { get; set; }

        [SqlMapper(ColumnName = nameof(PossuiVínculo))]
        public bool PossuiVínculo { get; set; }

        public override string ToString()
        {
            if (string.IsNullOrWhiteSpace(NomeIdentificador)) return Nome;
            else return $"{Nome} ({NomeIdentificador})";
        }
    }
}
