﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Gestora.Helpers;
using Gestora.Dados;

namespace Gestora.Fundos.Passivo
{
    public class Posição
    {
        [SqlMapper(ColumnName = nameof(ID))]
        public int ID { get; set; }

        [SqlMapper(ColumnName = nameof(DataConversão))]
        public DateTime DataConversão { get; set; }

        [SqlMapper(ColumnName = nameof(Fundo))]
        public Fundo Fundo { get; set; }

        public Distribuidor Distribuidor { get { return Cotista.Distribuidor; } }

        [SqlMapper(ColumnName = nameof(Cotas))]
        public double Cotas { get; set; }

        [SqlMapper(ColumnName = nameof(ValorAplicação))]
        public double ValorAplicação { get; set; }


        [SqlMapper(ColumnName = nameof(ValorCorrigido))]
        public double ValorCorrigido { get; set; }

        [SqlMapper(ColumnName = nameof(Cotista))]
        public Cotista Cotista { get; set; }

        public double ProvisãoIOF { get { return 0; } }

        public double AlíquotaIOF { get { return 0; } }

        public double PrazoDecorridoIOF { get { return 0; } }

        public double ProvisãoIR { get { return 0; } }

        public double AlíquotaIR { get { return 0; } }

        public double PrazoDecorridoIR { get { return 0; } }

        public double ValorResgate { get { return ValorCorrigido - (ProvisãoIOF + ProvisãoIR); } }

        public double Rendimento { get { return ValorResgate - ValorAplicação; } }

        public double Resultado { get { return Rendimento / ValorAplicação; } }

        public DateTime DataIsençãoIOF { get { return DataConversão.AddDays(30); } }
    }
}
