﻿using System.ComponentModel;

namespace Gestora.Fundos.Passivo
{
    /// <summary>
    /// Especifica a natureza da pessoa do <see cref="Cotista"/>.
    /// </summary>
    public enum TipoPessoa
    {
        /// <summary>
        /// O tipo da pessoa é desconhecido.
        /// </summary>
        [Description("Conta e Ordem")]
        ContaeOrdem,
        /// <summary>
        /// A pessoa é física.
        /// </summary>
        [Description("Pessoa Física")]
        Física,
        /// <summary>
        /// A pesssoa é jurídica.
        /// </summary>
        [Description("Pessoa Jurídica")]
        Jurídica
    }
    /// <summary>
    /// Especifica a direção da movimentação financeira da <see cref="Boleta"/>.
    /// </summary>
    public enum Operação
    {
        /// <summary>
        /// O <see cref="Cotista"/> está aplicando no ativo.
        /// </summary>
        [Description("Aplicação")]
        Aplicação,
        /// <summary>
        /// O <see cref="Cotista"/> está resgatando parte do seu saldo.
        /// </summary>
        [Description("Resgate parcial")]
        ResgateParcial,
        /// <summary>
        /// O <see cref="Cotista"/> está resgatando toda sua posição.
        /// </summary>
        [Description("Resgate total")] 
        ResgateTotal,
        /// <summary>
        /// A <see cref="Boleta"/> é de come-cotas.
        /// </summary>
        [Description("Recolhimento de impostos")] 
        RecolhimentoImpostos
    }
    /// <summary>
    /// Especifica o tipo do valor da <see cref="Boleta"/>.
    /// </summary>
    public enum TipoValor
    {
        /// <summary>
        /// A <see cref="Boleta"/> possui valor em moeda.
        /// </summary>
        [Description("Em moeda")] 
        EmMoeda,
        /// <summary>
        /// A <see cref="Boleta"/> possui valor em cotas.
        /// </summary>
        [Description("Em cotas")] 
        EmCotas
    }

    /// <summary>
    /// Especifica o status de uma <see cref="Boleta"/>.
    /// </summary>
    public enum Status
    {
        /// <summary>
        /// A <see cref="Boleta"/> está aprovada e será levada em conta na próxima validação de cota.
        /// </summary>
        [Description("Aprovado")] 
        Aprovado,
        /// <summary>
        /// A <see cref="Boleta"/> está reprovada e não será levada em conta na próxima validação de cota.
        /// </summary>
        [Description("Reprovado")] 
        Reprovado,
        /// <summary>
        /// A <see cref="Boleta"/> foi aprovada e validada, sendo considerada no total de cotas do fundo.
        /// </summary>
        [Description("Validado")] 
        Validado,
        /// <summary>
        /// A <see cref="Boleta"/> está em espera e será aprovada automaticamente na validação de cota.
        /// </summary>
        [Description("Em espera")] 
        EmEspera
    }
    
    /// <summary>
    /// Especifica a forma de liquidação da <see cref="Boleta"/> no <see cref="Gestora.Primitivos.Custodiante"/>.
    /// </summary>
    public enum FormaLiquidação
    {
        /// <summary>
        /// A <see cref="Boleta"/> é DOC ou TED.
        /// </summary>
        [Description("TED/DOC")] 
        DOCTED,
        /// <summary>
        /// A <see cref="Boleta"/> é CETIP.
        /// </summary>
        [Description("CETIP")] 
        CETIP
    }

    /// <summary>
    /// Especifica a forma de importação da <see cref="Boleta"/>.
    /// </summary>
    public enum FormaImportação
    {
        /// <summary>
        /// A forma de importação da <see cref="Boleta"/> é desconhecida.
        /// </summary>
        [Description("Desconhecida")]
        Desconhecida,
        /// <summary>
        /// A <see cref="Boleta"/> foi importada manualmente.
        /// </summary>
        [Description("Manual")]
        Manual,
        /// <summary>
        /// A <see cref="Boleta"/> foi importada automaticamente.
        /// </summary>
        [Description("Automática")]
        Automática
    }

    public enum Referência
    {
        [Description("Data de registro")]
        Registro,
        [Description("Data agendada")]
        Agendamento,
        [Description("Data de liquidação")]
        Liquidação,
        [Description("Data de cotização")]
        Cotização
    }
}
