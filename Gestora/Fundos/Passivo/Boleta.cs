﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Gestora.Helpers;
using Gestora.Dados;

namespace Gestora.Fundos.Passivo
{
    [SqlTableMapper(TableName = "Fundos", PrimaryKey = nameof(ID))]
    public class Boleta : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged members
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        #region Campos

        private DateTime datacotização;
        private DateTime dataliquidação;
        private DateTime dataagendada;
        private DateTime dataregistro;
        private Fundo fundo;
        private Operação operação;
        private double? valorfinanceiro;
        private double? cotas;
        private TipoValor tipovalor;
        private Cotista cotista;
        private FormaLiquidação formaliquidação;
        private string comentários;
        private Status status;
        private FormaImportação formaimportação;
        private bool cotaprevista;
        private bool valorprevisto;
        private bool isselected = false;

        #endregion

        [SqlMapper(ColumnName = nameof(ID))]
        public int ID { get; set; }

        [SqlMapper(ColumnName = nameof(DataCotização))]
        public DateTime DataCotização { get { return datacotização; } set { datacotização = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(DataLiquidação))]
        public DateTime DataLiquidação { get { return dataliquidação; } set { dataliquidação = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(DataAgendada))]
        public DateTime DataAgendada { get { return dataagendada; } set { dataagendada = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(DataRegistro))]
        public DateTime DataRegistro { get { return dataregistro; } set { dataregistro = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(Fundo))]
        public Fundo Fundo { get { return fundo; } set { fundo = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(Operação))]
        public Operação Operação { get { return operação; } set { operação = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(TipoValor))]
        public TipoValor TipoValor { get { return tipovalor; } set { tipovalor = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(ValorFinanceiro))]
        public double? ValorFinanceiro { get { return valorfinanceiro; } set { valorfinanceiro = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(Cotas))]
        public double? Cotas { get { return cotas; } set { cotas = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(Cotista))]
        public Cotista Cotista { get { return cotista; } set { cotista = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(FormaLiquidação))]
        public FormaLiquidação FormaLiquidação { get { return formaliquidação; } set { formaliquidação = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(Comentários))]
        public string Comentários { get { return comentários; } set { comentários = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(Status))]
        public Status Status { get { return status; } set { status = value; OnPropertyChanged(); } }
        [SqlMapper(ColumnName = nameof(Status))]
        public FormaImportação FormaImportação { get { return formaimportação; } set { formaimportação = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(CotaPrevista))]
        public bool CotaPrevista { get { return cotaprevista; } set { cotaprevista = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(ValorPrevisto))]
        public bool ValorPrevisto { get { return valorprevisto; } set { valorprevisto = value; OnPropertyChanged(); } }

        public bool IsSelected { get { return isselected; } set { isselected = value; OnPropertyChanged(); } }
    }
}
