﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Gestora.Dados;
using Gestora.Primitivos;

namespace Gestora.Fundos.Passivo
{
    [SqlTableMapper(TableName = "Distribuidores", PrimaryKey = nameof(Código))]
    public class Distribuidor : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #region Campos

        private string código;
        private string razãosocial;
        private string prefixo;
        private ContaBancária contaaplicação;
        private ContaBancária contaresgate;
        private ContaBancária contarebate;
        private bool válido;

        #endregion

        [SqlMapper(ColumnName = nameof(Código))]
        public string Código { get { return código; } set { código = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(RazãoSocial))]
        public string RazãoSocial { get { return razãosocial; } set { razãosocial = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(Prefixo))]
        public string Prefixo { get { return prefixo; } set { prefixo = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(ContaAplicação))]
        public ContaBancária ContaAplicação { get { return contaaplicação; } set { contaaplicação = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(ContaResgate))]
        public ContaBancária ContaResgate { get { return contaresgate; } set { contaresgate = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(ContaRebate))]
        public ContaBancária ContaRebate { get { return contarebate; } set { contarebate = value; OnPropertyChanged(); } }

        [SqlMapper(ColumnName = nameof(Válido))]
        public bool Válido { get { return válido; } set { válido = value; OnPropertyChanged(); } }

        public override string ToString()
        {
            return RazãoSocial;
        }
    }
}
