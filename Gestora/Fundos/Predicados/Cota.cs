﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Gestora.Primitivos;
using Gestora.Fundos.Passivo;
using System.Collections.ObjectModel;
using Gestora.Dados;

namespace Gestora.Fundos.Predicados
{
    [SqlTableMapper(TableName = "Cotas", PrimaryKeys = new string[2] { nameof(Data), nameof(Fundo) })]
    public class Cota
    {
        [SqlMapper(ColumnName = nameof(Data))]
        public DateTime Data { get; set; }

        [SqlMapper(ColumnName = nameof(Fundo))]
        public Fundo Fundo { get; set; }

        [SqlMapper(ColumnName = nameof(Cotação))]
        public double Cotação { get; set; }

        [SqlMapper(ColumnName = nameof(Status))]
        public Status Status { get; set; }
    }
}
