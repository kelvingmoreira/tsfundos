﻿using System.ComponentModel;

namespace Gestora.Fundos
{
    /// <summary>
    /// 
    /// </summary>
    public enum Status
    {
        /// <summary>
        /// A <see cref="Boleta"/> está aprovada e será levada em conta na próxima validação de cota.
        /// </summary>
        [Description("Em aberto")]
        EmAberto,
        /// <summary>
        /// A <see cref="Boleta"/> está reprovada e não será levada em conta na próxima validação de cota.
        /// </summary>
        [Description("Validado")]
        Validado
    }
}
