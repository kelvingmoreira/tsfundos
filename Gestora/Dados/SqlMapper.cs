﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestora.Dados
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    [Serializable]
    public class SqlMapper : Attribute
    {
        public string ColumnName = null;
    }

    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    [Serializable]
    public class SqlTableMapper : Attribute
    {
        public string TableName = null;
        public string PrimaryKey = null;
        public string[] PrimaryKeys = null;
    }

    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    [Serializable]
    public class SqlProcedureMapper : Attribute
    {
        public string ProcedureName = null;
    }
}
