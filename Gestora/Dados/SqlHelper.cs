﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Gestora.Dados
{

    public class SqlHelper
    {

        public static string ConexãoString = "Data Source=172.16.100.4;Initial Catalog=TSFundosDB;Integrated Security=SSPI;";

        /// <summary>
        /// Realiza uma consulta ao servidor SQL e devolve a tabela.
        /// </summary>
        /// <param name="query">A consulta ao banco de dados.</param>
        /// <returns>Retorna uma <see cref="DataTable"/>.</returns>
        public DataTable DataTableFromQuery(string query)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter consultaSQL = new SqlDataAdapter(query, ConexãoString);
            consultaSQL.Fill(dt);
            return dt;
        }

        private T MapToClass<T>(SqlDataReader reader) where T : class
        {
            T returnedObject = Activator.CreateInstance<T>();
            PropertyInfo[] modelProperties = returnedObject.GetType().GetProperties();
            for (int i = 0; i < modelProperties.Length; i++)
            {
                SqlMapper[] attributes = modelProperties[i].GetCustomAttributes<SqlMapper>(true).ToArray();

                if (attributes.Length > 0 && attributes[0].ColumnName != null)
                {
                    if (reader[attributes[0].ColumnName].GetType() != typeof(DBNull))
                        modelProperties[i].SetValue(returnedObject, Convert.ChangeType(reader[attributes[0].ColumnName], modelProperties[i].PropertyType), null);
                }
            }
            return returnedObject;
        }

        /// <summary>
        /// Retorna uma coleção de classes de uma consulta no servidor SQL, se a consulta corresponder ao tipo T e a classe for mapeada usando <see cref="SqlMapper"/>.
        /// </summary>
        /// <typeparam name="T">O Tipo do objeto que será buscado</typeparam>
        /// <param name="Query">A query.</param>
        /// <returns>Devolve uma <see cref="ObservableCollection"/></returns>
        public ObservableCollection<T> GetObjectsFromSQL<T>(string Query) where T : class
        {
            ObservableCollection<T> collection = new ObservableCollection<T>();
            using (SqlConnection myConnection = new SqlConnection(ConexãoString))
            {
                SqlCommand oCmd = new SqlCommand(Query, myConnection);
                myConnection.Open();
                using (SqlDataReader oReader = oCmd.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        T obj = Activator.CreateInstance<T>();
                        obj = MapToClass<T>(oReader);
                        collection.Add(obj);
                    }
                    myConnection.Close();
                }
            }
            return collection;
        }
    }
}
