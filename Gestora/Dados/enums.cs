﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestora.Dados
{
    public enum ManipulationType
    {
        Insert,
        Update,
        Delete
    }
}
