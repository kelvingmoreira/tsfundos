﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.ComponentModel;

namespace Gestora.Dados
{
    public abstract class DataServicer<T> : INotifyPropertyChanged, IDataErrorInfo where T : class
    {
        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        #region IDataErrorInfo Members

        public string Error
        {
            get
            {
                if (ValidationErrors.Count > 0)
                {
                    return $"{ValidationErrors.Count} erro{(ValidationErrors.Count == 1 ? "" : "s")} encontrado{(ValidationErrors.Count == 1 ? "" : "s")}";
                }
                return null;
            }
        }

        public string this[string columnName]
        {
            get
            {
                if (ValidationErrors.ContainsKey(columnName))
                {
                    return ValidationErrors[columnName];
                }
                return null;
            }
        }

        #endregion

        internal protected string ConexãoString = SqlHelper.ConexãoString;
        public ManipulationType? ManipulationType { get; set; }
        protected string InsertProcedureName;
        protected string UpdateProcedureName;
        protected string DeleteProcedureName;
        internal List<SqlParameter> ParameterList { get; set; }
        private SqlCommand Procedure { get; set; }
        public Dictionary<string, string> ValidationErrors = new Dictionary<string, string>();

        public DataServicer()
        {
            ParameterList = new List<SqlParameter>();
            AddProcedureParameters();
        }

        public abstract void Validate();
        protected abstract void AddProcedureParameters();
        protected abstract void InsertParameterValues();

        public void ExecuteNonQuery()
        {
            if (ManipulationType == null) throw new Exception("Manipulation type not defined.");
            Validate();
            if (ValidationErrors.Count != 0) throw new Exception("Validation errors detected.");
            InsertParameterValues();
            using (SqlConnection myConnection = new SqlConnection(ConexãoString))
            {
                myConnection.Open();
                if (ManipulationType ==  Dados.ManipulationType.Insert) Procedure = new SqlCommand(InsertProcedureName, myConnection);
                if (ManipulationType == Dados.ManipulationType.Update) Procedure = new SqlCommand(UpdateProcedureName, myConnection);
                if (ManipulationType == Dados.ManipulationType.Delete) Procedure = new SqlCommand(DeleteProcedureName, myConnection);
                Procedure.Parameters.AddRange(ParameterList.ToArray());
                Procedure.CommandType = CommandType.StoredProcedure;
                Procedure.ExecuteNonQuery();
                Procedure.Parameters.Clear();
            }
        }
    }
}
