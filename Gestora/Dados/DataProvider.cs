﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.ComponentModel;

namespace Gestora.Dados
{
    public abstract class DataProvider<T> : INotifyPropertyChanged where T : class
    {
        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        private string ConexãoString = SqlHelper.ConexãoString;
        internal string ProcedureName;
        private SqlCommand Procedure { get; set; }
        internal List<SqlParameter> ParameterList { get; set; }
        public ObservableCollection<T> Collection { get; private set; }

        public DataProvider()
        {
            ParameterList = new List<SqlParameter>();
            AddProcedureParameters();
        }

        public string GetColumnName(string propertyname = null)
        {
            SqlMapper[] attributes = typeof(T).GetProperty(propertyname).GetCustomAttributes<SqlMapper>(true).ToArray();
            if (attributes.Length == 0 || attributes[0].ColumnName == null)
            {
                throw new Exception("A propriedade não possui o SqlMapper declarado corretamente.");
            }
            return attributes[0].ColumnName;
        }

        private void GetValueTypes(T obj, SqlDataReader reader)
        {
            PropertyInfo[] modelProperties = obj.GetType().GetProperties();
            for (int i = 0; i < modelProperties.Length; i++)
            {
                SqlMapper[] attributes = modelProperties[i].GetCustomAttributes<SqlMapper>(true).ToArray();

                if (attributes.Length > 0 && attributes[0].ColumnName != null)
                {
                    if (reader[attributes[0].ColumnName].GetType() != typeof(DBNull) && modelProperties[i].PropertyType.IsValueType)
                    {
                        if (modelProperties[i].PropertyType.IsEnum)
                            modelProperties[i].SetValue(obj, CastPropertyValue(modelProperties[i], reader[attributes[0].ColumnName].ToString()), null);
                        else
                        {
                            Type propertyType = modelProperties[i].PropertyType;
                            propertyType = Nullable.GetUnderlyingType(propertyType) ?? propertyType;
                            modelProperties[i].SetValue(obj, Convert.ChangeType(reader[attributes[0].ColumnName], propertyType), null);
                        }
                            
                    }
                    else if (reader[attributes[0].ColumnName].GetType() != typeof(DBNull) && modelProperties[i].PropertyType == typeof(string))
                    {
                        modelProperties[i].SetValue(obj, CastPropertyValue(modelProperties[i], reader[attributes[0].ColumnName].ToString()), null);
                    }
                }
            }
        }

        public abstract void GetReferenceTypes(T obj, SqlDataReader reader);
        public abstract void AddProcedureParameters();
        public virtual void InsertParameterValues() { }

        public object CastPropertyValue(PropertyInfo property, string value)
        {
            if (property == null || String.IsNullOrEmpty(value))
                return null;
            if (property.PropertyType.IsEnum)
            {
                Type enumType = property.PropertyType;
                if (Enum.IsDefined(enumType, int.Parse(value)))
                    return Enum.Parse(enumType, value);
                else
                    return null;
            }
            else
                return Convert.ChangeType(value, property.PropertyType);
        }

        public void RunSearch()
        {
            CriarColeção(); 
            OnPropertyChanged(nameof(Collection));
        }

        public Task RunSearchAsync()
        {
            return Task.Run(() => { CriarColeção(); OnPropertyChanged(nameof(Collection)); });
        }

        private void CriarColeção()
        {
            Collection = new ObservableCollection<T>();
            using (SqlConnection myConnection = new SqlConnection(ConexãoString))
            {
                myConnection.Open();
                Procedure = new SqlCommand(ProcedureName, myConnection);
                InsertParameterValues();
                Procedure.Parameters.AddRange(ParameterList.ToArray());
                Procedure.CommandType = CommandType.StoredProcedure;

                using (SqlDataReader oReader = Procedure.ExecuteReader())
                {
                    while (oReader.Read())
                    {
                        T obj = Activator.CreateInstance<T>();
                        GetValueTypes(obj, oReader);
                        GetReferenceTypes(obj, oReader);
                        Collection.Add(obj);
                    }
                    myConnection.Close();
                }
                Procedure.Parameters.Clear();
            }
        }
    }
}
