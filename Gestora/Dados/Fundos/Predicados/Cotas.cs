﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Reflection;
using Gestora.Dados.Primitivos;
using Gestora.Fundos.Predicados;
using Gestora.Fundos;
using Gestora.Primitivos;
using Gestora.Dados.Fundos.Passivo;
using System.Data.SqlTypes;
using Gestora.Dados.Fundos;
using Gestora.Helpers;

namespace Gestora.Dados.Fundos.Predicados
{
    /// <summary>
    /// Fornece métodos de provisão de dados do tipo <see cref="Cota"/>.
    /// </summary>
    /// <remarks>
    /// Obs.: O construtor <see cref="CotasProvider()"/> não cria a coleção de dados. 
    /// </remarks>
    public class CotasProvider : DataProvider<Cota>
    {
        #region Campos

        private Fundo fundo;
        private DateTime? de;
        private DateTime? até;

        #endregion

        #region Propriedades

        public ObservableCollection<Fundo> Fundos { get; set; }
        public Fundo Fundo { get { return fundo; } set { fundo = value; OnPropertyChanged(); } }
        public DateTime? De 
        { 
            get 
            { 
                if(Collection != null) 
                    if (Collection.Count != 0)
                        return Collection.First().Data;
                    else
                        return de;
                else
                    return de;
            } 
            set { de = value; OnPropertyChanged(); }
        }
        public DateTime? Até 
        {
            get
            {
                if (Collection != null)
                    if (Collection.Count != 0)
                        return Collection.Last().Data;
                    else
                        return até;
                else
                    return até;
            }
            set { até = value; OnPropertyChanged(); } 
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Inicializa uma nova instância da classe <see cref="CotasProvider"/>
        /// </summary>
        public CotasProvider() 
        { 
            Fundos = new Fundos().Collection; 
        }

        public override void AddProcedureParameters()
        {
            ProcedureName = "[dbo].[BuscarCotas]";
            ParameterList.Add(new SqlParameter("@Fundo", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@De", SqlDbType.Date));
            ParameterList.Add(new SqlParameter("@Até", SqlDbType.Date));
        }

        public override void InsertParameterValues()
        {
            ParameterList.Single(x => x.ParameterName == "@Fundo").Value = Fundo.Código;
            if (De.HasValue) ParameterList.Single(x => x.ParameterName == "@De").Value = De.Value; else ParameterList.Single(x => x.ParameterName == "@De").Value = null;
            if (Até.HasValue) ParameterList.Single(x => x.ParameterName == "@Até").Value = Até.Value; else ParameterList.Single(x => x.ParameterName == "@Até").Value = null;
        }

        public override void GetReferenceTypes(Cota obj, SqlDataReader reader)
        {
            PropertyInfo property;
            SqlMapper[] attributes;
            string foreignkey;

            property = obj.GetType().GetProperty(nameof(Fundo));
            attributes = property.GetCustomAttributes<SqlMapper>(true).ToArray();
            foreignkey = reader[attributes[0].ColumnName].ToString();
            if (Fundo != null) obj.Fundo = Fundo;
            else obj.Fundo = Fundos.Single(x => x.Código.ToString() == foreignkey);
        }

        #endregion
    }

    public class CotasServicer : DataServicer<Cota>
    {
        #region Validação

        public override void Validate()
        {
            ValidationErrors.Clear();

            bool isInputValid = true;

            #region Validações de input

            if (Fundo == null)
            { ValidationErrors.Add(nameof(Fundo), "É preciso escolher um fundo."); isInputValid = false; }
            if (DataCotização == null)
            { ValidationErrors.Add(nameof(DataCotização), "É preciso escolher uma data."); isInputValid = false; }
            if (Cotação == null || Cotação == 0)
            { ValidationErrors.Add(nameof(Cotação), "Valor inválido."); isInputValid = false; }

            #endregion

            #region Validações lógicas

            if (isInputValid)
            {
                if (!DataCotização.Value.IsWorkDay())
                { ValidationErrors.Add(nameof(DataCotização), "A data não é um dia útil."); isInputValid = false; }
                else if (Cotas.Collection.SingleOrDefault(x => x.Data == DataCotização.Value)?.Status == Status.Validado)
                { ValidationErrors.Add(nameof(DataCotização), "A data já possui valor de cota."); isInputValid = false; }
                else if (Cotas.Collection.Last().Data.GetNextWorkDay() < DataCotização)
                { ValidationErrors.Add(nameof(DataCotização), $"A data {Cotas.Collection.Last().Data.GetNextWorkDay():d} requer valor de cota antes desta data."); isInputValid = false; }
            }

            #endregion

            OnPropertyChanged(null);
        }

        #endregion

        #region Campos

        private Fundo fundo;
        private double? cotação;
        private DateTime? datacotização;
        private CotasProvider cotas;
        #endregion

        #region Propriedades

        public ObservableCollection<Fundo> Fundos { get; set; }
        public Fundo Fundo 
        { 
            get { return fundo; } 
            set 
            { 
                fundo = value;
                Cotas = new CotasProvider { Fundo = Fundo };
                Cotas.RunSearch();
                DataCotização = Cotas.Até.Value.GetNextWorkDay();
                OnPropertyChanged(null); 
            } 
        }
        public double? Cotação { get { return cotação; } set { cotação = value; OnPropertyChanged(null); } }
        public DateTime? DataCotização { get { return datacotização; } set { datacotização = value; OnPropertyChanged(null); } }
        public CotasProvider Cotas { get { return cotas; } set { cotas = value; OnPropertyChanged(); } }
        public DateTime? DataInício { get { return Cotas?.De; } }
        public DateTime? DataFim { get { return Cotas?.Até.Value.GetNextWorkDay(); } }
        public double? QtdCotas
        {
            get
            {
                if (Fundo == null || DataCotização == null || Cotação == null) return null;
                double valor;
                using (SqlConnection myConnection = new SqlConnection(ConexãoString))
                {
                    myConnection.Open();
                    SqlCommand sqlCommand = new SqlCommand("[dbo].[BuscarQtdCotas]", myConnection);
                    sqlCommand.Parameters.Add("@Fundo", SqlDbType.VarChar, 100).Value = Fundo.Código;
                    sqlCommand.Parameters.Add("@Cotação", SqlDbType.Float).Value = Cotação;
                    sqlCommand.Parameters.Add("@DataCotização", SqlDbType.Date).Value = DataCotização.Value;
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader oReader = sqlCommand.ExecuteReader())
                    {
                        oReader.Read();
                        valor = double.Parse(oReader[0].ToString());
                        myConnection.Close();
                    }
                }
                return valor;
            }
        }

        public double? PatrimônioLiquido
        {
            get
            {
                if (QtdCotas == null) return null;
                return QtdCotas * Cotação;
            }
        }

        #endregion

        #region Métodos

        public CotasServicer()
        {
            Fundos = new Fundos().Collection;
            ManipulationType = Dados.ManipulationType.Insert;
        }

        protected override void AddProcedureParameters()
        {
            InsertProcedureName = "[dbo].[InserirCota]";
            UpdateProcedureName = "[dbo].[InserirCota]";
            ParameterList.Add(new SqlParameter("@Fundo", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@Cotação", SqlDbType.Float));
            ParameterList.Add(new SqlParameter("@DataCotização", SqlDbType.Date));
        }

        protected override void InsertParameterValues()
        {
            ParameterList.Single(x => x.ParameterName == "@Fundo").Value = Fundo.Código;
            ParameterList.Single(x => x.ParameterName == "@Cotação").Value = Cotação.Value;
            ParameterList.Single(x => x.ParameterName == "@DataCotização").Value = DataCotização;
        }

        #endregion
    }
}
