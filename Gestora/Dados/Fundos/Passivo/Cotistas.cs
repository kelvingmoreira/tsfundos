﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Reflection;
using Gestora.Dados.Primitivos;
using Gestora.Fundos;
using Gestora.Fundos.Passivo;
using System.Collections.Generic;
using Gestora.Primitivos;
using System.Collections.ObjectModel;
using Gestora.Helpers;

namespace Gestora.Dados.Fundos.Passivo
{
    /// <summary>
    /// Fornece métodos de provisão de dados do tipo <see cref="Cotista"/>.
    /// </summary>
    /// <remarks>
    /// Obs.: O construtor <see cref="CotistasProvider()"/> não cria a coleção de dados. 
    /// </remarks>
    public class CotistasProvider : DataProvider<Cotista>
    {
        private Distribuidores Distribuidores { get; set; }
        protected ContasBancáriasProvider ContasBancárias { get; set; }

        private string NomeCotista { get; set; }

        /// <summary>
        /// Inicializa uma nova instância da classe <see cref="CotistasProvider"/>
        /// </summary>
        public CotistasProvider()
        {
            Distribuidores = new Distribuidores();
            ContasBancárias = new ContasBancáriasProvider();
        }

        /// <summary>
        /// Inicializa uma nova instância da classe <see cref="CotistasProvider"/> com apenas um <see cref="Cotista"/> em <see cref="DataProvider{T}.Collection"/>.
        /// </summary>
        /// <param name="cotista">O nome do <see cref="Cotista"/> que se deseja buscar.</param>
        public CotistasProvider(string cotista)
        {
            if (string.IsNullOrEmpty(cotista)) throw new ArgumentException("O cotista não pode ser nulo");
            Distribuidores = new Distribuidores();
            ContasBancárias = new ContasBancáriasProvider();
            NomeCotista = cotista;
            RunSearch();
        }

        public override void AddProcedureParameters()
        {
            ProcedureName = "[dbo].[BuscarCotistas]";
            ParameterList.Add(new SqlParameter("@Nome", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@NomeIdentificador", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@TipoPessoa", SqlDbType.SmallInt));
            ParameterList.Add(new SqlParameter("@Distribuidor", SqlDbType.VarChar, 100));
        }

        public override void InsertParameterValues()
        {
            if (NomeCotista != null) ParameterList.Single(x => x.ParameterName == "@Nome").Value = NomeCotista;
        }

        public override void GetReferenceTypes(Cotista obj, SqlDataReader reader)
        {
            PropertyInfo property;
            SqlMapper[] attributes;
            string foreignkey;

            property = obj.GetType().GetProperty(nameof(Distribuidor));
            attributes = property.GetCustomAttributes<SqlMapper>(true).ToArray();
            foreignkey = reader[attributes[0].ColumnName].ToString();

            obj.Distribuidor = Distribuidores.Collection.FirstOrDefault(x => x.Código.ToString() == foreignkey);

            property = obj.GetType().GetProperty(nameof(Cotista.ContaBancária));
            attributes = property.GetCustomAttributes<SqlMapper>(true).ToArray();
            foreignkey = reader[attributes[0].ColumnName].ToString();

            obj.ContaBancária = ContasBancárias.Collection.FirstOrDefault(x => x.ID.ToString() == foreignkey);

            //Falta implementar busca de transferências
        }
    }

    public class CotistasServicer : DataServicer<Boleta>
    {
        #region Validação


        public new string Error
        {
            get
            {
                if (ValidationErrors.Count > 0 || ContaBancária?.ValidationErrors.Count > 0)
                {
                    int erros = ValidationErrors.Count + (ContaBancária?.ValidationErrors.Count).GetValueOrDefault();
                    return $"{erros} erro{(erros == 1 ? "" : "s")} encontrado{(erros == 1 ? "" : "s")}";
                }
                return null;
            }
        }

        public override void Validate()
        {
            ValidationErrors.Clear();
            ContaBancária?.ValidationErrors.Clear();

            bool isInputValid = true;

            #region Validações de input

            if (TipoPessoa == Gestora.Fundos.Passivo.TipoPessoa.Física)
            {
                if (string.IsNullOrWhiteSpace(Nome))
                { ValidationErrors.Add(nameof(Nome), "É preciso incluir o nome."); isInputValid = false; }
                if (string.IsNullOrWhiteSpace(Cpf))
                { ValidationErrors.Add(nameof(Cpf), "É preciso incluir o CPF."); isInputValid = false; }
                ContaBancária.Validate();
            }
            else if (TipoPessoa == Gestora.Fundos.Passivo.TipoPessoa.Jurídica)
            {
                if (string.IsNullOrWhiteSpace(Nome))
                { ValidationErrors.Add(nameof(Nome), "É preciso incluir o nome."); isInputValid = false; }
                if (string.IsNullOrWhiteSpace(Cnpj))
                { ValidationErrors.Add(nameof(Cnpj), "É preciso incluir o CNPJ."); isInputValid = false; }
                ContaBancária.Validate();
            }
            else
            {
                if (Distribuidor == null)
                { ValidationErrors.Add(nameof(Distribuidor), "É preciso selecionar um distribuidor."); isInputValid = false; }
                else if (!Distribuidor.Válido)
                { ValidationErrors.Add(nameof(Distribuidor), $"{Distribuidor} não é um distribuidor válido."); isInputValid = false; }
                else if (string.IsNullOrWhiteSpace(CódigoCotista))
                { ValidationErrors.Add(nameof(CódigoCotista), "É preciso incluir o código do cotista."); isInputValid = false; }
            }

            #endregion

            #region Validações lógicas

            if (isInputValid)
            {
                if (TipoPessoa == Gestora.Fundos.Passivo.TipoPessoa.Física)
                {
                    if (!Cpf.IsCpf()) ValidationErrors.Add(nameof(Cpf), "O CPF não é valido.");
                    else
                    {
                        CotistasProvider cotistas = new CotistasProvider();
                        cotistas.RunSearch();
                        if (cotistas.Collection.SingleOrDefault(x => x.Cpf == Cpf && x.ID != ID && ID != null) != null)
                            ValidationErrors.Add(nameof(Cpf), "O CPF já está cadastrado.");
                    }
                    if (Nome.Length < 5) ValidationErrors.Add(nameof(Nome), "O nome é muito curto.");
                }
                else if (TipoPessoa == Gestora.Fundos.Passivo.TipoPessoa.Jurídica)
                {
                    if (!Cnpj.IsCnpj()) ValidationErrors.Add(nameof(Cnpj), "O CNPJ não é valido.");
                    else
                    {
                        CotistasProvider cotistas = new CotistasProvider();
                        cotistas.RunSearch();
                        if (cotistas.Collection.SingleOrDefault(x => x.Cnpj == Cnpj && x.ID != ID && ID != null) != null)
                            ValidationErrors.Add(nameof(Cnpj), "O CNPJ já está cadastrado.");
                    }
                    if (Nome.Length < 5) ValidationErrors.Add(nameof(Nome), "O nome é muito curto.");
                }
                else
                {
                    ObservableCollection<Cotista> c = new CotistasProvider($"{Distribuidor.Prefixo} {CódigoCotista}").Collection;
                    if (c.Count != 0 && c.SingleOrDefault(x => x.ID != ID && ID != null) != null) 
                        ValidationErrors.Add(nameof(CódigoCotista), "O cotista já está cadastrado.");
                    if (NomeIdentificador?.Length != 0 && NomeIdentificador?.Length < 5)
                        ValidationErrors.Add(nameof(NomeIdentificador), "O nome é muito curto.");
                }
            }


            #endregion

            OnPropertyChanged(null);
        }

        #endregion

        #region Campos

        private string nome;
        private string nomeidentificador;
        private string códigocotista;
        private TipoPessoa? tipopessoa;
        private string cpf;
        private string cnpj;
        private Distribuidor distribuidor;
        private ContasBancáriasServicer contabancária;

        #endregion

        #region Propriedades

        public ObservableCollection<Distribuidor> Distribuidores { get; set; }

        public int? ID { get; set; }
        public string Nome { get { return nome; } set { nome = value?.ToUpper(); OnPropertyChanged(); } }
        public string NomeIdentificador { get { return nomeidentificador; } set { nomeidentificador = value?.ToUpper(); OnPropertyChanged(); } }
        public string CódigoCotista { get { return códigocotista; } set { códigocotista = value; OnPropertyChanged(); } }
        public TipoPessoa? TipoPessoa 
        { 
            get 
            { return tipopessoa; } 
            set 
            { 
                tipopessoa = value; 
                ValidationErrors.Clear();
                if (TipoPessoa != Gestora.Fundos.Passivo.TipoPessoa.ContaeOrdem)
                {
                    Nome = null;
                    Cpf = null;
                    Cnpj = null;
                    ContaBancária.Nome = null;
                    ContaBancária.Banco = null;
                    ContaBancária.Agência = null;
                    ContaBancária.Conta = null;
                    ContaBancária.Dígito = null;
                    ContaBancária.ValidationErrors.Clear();
                }
                else
                {
                    Distribuidor = null;
                    CódigoCotista = null;
                    NomeIdentificador = null;
                    if (TipoPessoa == Gestora.Fundos.Passivo.TipoPessoa.Física) Cnpj = null;
                    else Cpf = null;

                }
                OnPropertyChanged(null);
            } 
        }
        public string Cpf { get { return cpf; } set { cpf = value; OnPropertyChanged(); } }
        public string Cnpj { get { return cnpj; } set { cnpj = value; OnPropertyChanged(); } }
        public Distribuidor Distribuidor { get { return distribuidor; } set { distribuidor = value; OnPropertyChanged(); } }
        public ContasBancáriasServicer ContaBancária { get { return contabancária; } set { contabancária = value; OnPropertyChanged(); } }

        #endregion

        #region Métodos

        public CotistasServicer()
        {
            Distribuidores = new Distribuidores().Collection;
            ContaBancária = new ContasBancáriasServicer();
            TipoPessoa = Gestora.Fundos.Passivo.TipoPessoa.ContaeOrdem;
            ManipulationType = Dados.ManipulationType.Insert;
        }

        protected override void AddProcedureParameters()
        {
            InsertProcedureName = "[dbo].[InserirCotista]";
            UpdateProcedureName = "[dbo].[AlterarCotista]";
            DeleteProcedureName = UpdateProcedureName;
            ParameterList.Add(new SqlParameter("@ManipulationType", SqlDbType.SmallInt));
            ParameterList.Add(new SqlParameter("@ID", SqlDbType.Int));
            ParameterList.Add(new SqlParameter("@Nome", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@NomeIdentificador", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@TipoPessoa", SqlDbType.SmallInt));
            ParameterList.Add(new SqlParameter("@CPF", SqlDbType.NChar, 14));
            ParameterList.Add(new SqlParameter("@CNPJ", SqlDbType.NChar, 18));
            ParameterList.Add(new SqlParameter("@Distribuidor", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@Transferência", SqlDbType.Int));
            ParameterList.Add(new SqlParameter("@ContaBancária", SqlDbType.SmallInt));
        }

        protected override void InsertParameterValues()
        {
            ParameterList.Single(x => x.ParameterName == "@ManipulationType").Value = (int)ManipulationType;
            ParameterList.Single(x => x.ParameterName == "@ID").Value = ID;
            if (TipoPessoa == Gestora.Fundos.Passivo.TipoPessoa.ContaeOrdem)
            {
                ParameterList.Single(x => x.ParameterName == "@Nome").Value = $"{Distribuidor.Prefixo} {CódigoCotista}";
                ParameterList.Single(x => x.ParameterName == "@NomeIdentificador").Value = string.IsNullOrWhiteSpace(NomeIdentificador) ? null : NomeIdentificador;
                ParameterList.Single(x => x.ParameterName == "@TipoPessoa").Value = (int)TipoPessoa;
                ParameterList.Single(x => x.ParameterName == "@Distribuidor").Value = Distribuidor.Código;
                ParameterList.Single(x => x.ParameterName == "@Transferência").Value = null;
                ParameterList.Single(x => x.ParameterName == "@ContaBancária").Value = null;
            }
            else if (TipoPessoa == Gestora.Fundos.Passivo.TipoPessoa.Física)
            {
                ContaBancária.Nome = $"Conta bancária de {Nome}";
                ContaBancária.ExecuteNonQuery();
                ParameterList.Single(x => x.ParameterName == "@Nome").Value = Nome;
                ParameterList.Single(x => x.ParameterName == "@TipoPessoa").Value = (int)TipoPessoa;
                ParameterList.Single(x => x.ParameterName == "@CPF").Value = Cpf;
                ParameterList.Single(x => x.ParameterName == "@Distribuidor").Value = new Distribuidores().Collection.Single(x => x.Código == Global.Instancia.GestoraCnpj).Código;
                ParameterList.Single(x => x.ParameterName == "@Transferência").Value = null;
                ParameterList.Single(x => x.ParameterName == "@ContaBancária").Value = 
                    new ContasBancáriasProvider().Collection.Single(x => (x.Banco == ContaBancária.Banco && x.Agência == ContaBancária.Agência && x.Conta == ContaBancária.Conta && x.Dígito == ContaBancária.Dígito)).ID;
            }
            else
            {
                ContaBancária.Nome = $"Conta bancária de {Nome}";
                ContaBancária.ExecuteNonQuery();
                ParameterList.Single(x => x.ParameterName == "@Nome").Value = Nome;
                ParameterList.Single(x => x.ParameterName == "@TipoPessoa").Value = (int)TipoPessoa;
                ParameterList.Single(x => x.ParameterName == "@CNPJ").Value = Cnpj;
                ParameterList.Single(x => x.ParameterName == "@Distribuidor").Value = new Distribuidores().Collection.Single(x => x.Código == Global.Instancia.GestoraCnpj).Código;
                ParameterList.Single(x => x.ParameterName == "@Transferência").Value = null;
                ParameterList.Single(x => x.ParameterName == "@ContaBancária").Value =
                    new ContasBancáriasProvider().Collection.First(x => (x.Banco == ContaBancária.Banco && x.Agência == ContaBancária.Agência && x.Conta == ContaBancária.Conta && x.Dígito == ContaBancária.Dígito)).ID;
            }
        }

        #endregion
    }
}
