﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;
using Gestora.Dados;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlTypes;
using Gestora.Fundos;
using Gestora.Fundos.Predicados;
using Gestora.Dados.Fundos.Predicados;
using Gestora.Helpers;
using System.Collections.Generic;


namespace Gestora.Dados.Fundos.Passivo
{
    using Gestora.Fundos.Passivo;

    /// <summary>
    /// Fornece métodos de provisão de dados do tipo <see cref="Boleta"/>.
    /// </summary>
    /// <remarks>
    /// Obs.: O construtor <see cref="BoletasProvider()"/> não cria a coleção de dados. 
    /// </remarks>
    public class BoletasProvider : DataProvider<Boleta>, IDataErrorInfo
    {
        #region IDataErrorInfo Members

        public string Error
        {
            get
            {
                if (ValidationErrors.Count > 0)
                {
                    return $"{ValidationErrors.Count} erro{(ValidationErrors.Count == 1 ? "" : "s")}";
                }
                return null;
            }
        }

        public string this[string columnName]
        {
            get
            {
                if (ValidationErrors.ContainsKey(columnName))
                {
                    return ValidationErrors[columnName];
                }
                return null;
            }
        }

        #endregion

        #region Validação

        protected Dictionary<string, string> ValidationErrors = new Dictionary<string, string>();

        protected void Validate()
        {
            ValidationErrors.Clear();

            bool isInputValid = true;

            #region Validações de input

            if (Fundo == null)
            { ValidationErrors.Add(nameof(Fundo), "É preciso escolher um fundo."); isInputValid = false; }

            #endregion

            #region Validações lógicas

            if (isInputValid)
            {

            }

            #endregion

            OnPropertyChanged(null);
        }

        #endregion

        #region Campos

        private Fundo fundo;
        private Distribuidor distribuidor;
        private Cotista cotista;
        private Operação? operação;
        private Referência referência = Referência.Registro;
        private DateTime? de = DateTime.Today;
        private DateTime? até = DateTime.Today;
        private bool aprovadas = true;
        private bool reprovadas;
        private bool validadas = true;
        private bool emespera = true;

        #endregion

        #region Propriedades

        public ObservableCollection<Fundo> Fundos { get; set; }
        public ObservableCollection<Cotista> Cotistas { get; set; }
        public int? ID { get; set; }
        public Fundo Fundo { get { return fundo; } set { fundo = value; OnPropertyChanged(); } }
        public Distribuidor Distribuidor { get { return distribuidor; } set { distribuidor = value; OnPropertyChanged(); } }
        public Cotista Cotista { get { return cotista; } set { cotista = value; OnPropertyChanged(); } }
        public Operação? Operação { get { return operação; } set { operação = value; OnPropertyChanged(); } }
        public Referência Referência { get { return referência; } set { referência = value; OnPropertyChanged(); } }
        public DateTime? De { get { return de; } set { de = value; OnPropertyChanged(); } }
        public DateTime? Até { get { return até; } set { até = value; OnPropertyChanged(); } }
        public bool Aprovadas { get { return aprovadas; } set { aprovadas = value; OnPropertyChanged(); } }
        public bool Reprovadas { get { return reprovadas; } set { reprovadas = value; OnPropertyChanged(); } }
        public bool Validadas { get { return validadas; } set { validadas = value; OnPropertyChanged(); } }
        public bool EmEspera { get { return emespera; } set { emespera = value; OnPropertyChanged(); } }

        #endregion

        #region Métodos

        /// <summary>
        /// Inicializa uma nova instância da classe <see cref="BoletasProvider"/>
        /// </summary>
        public BoletasProvider()
        {
            Fundos = new Fundos().Collection;
        }

        public override void AddProcedureParameters()
        {
            ProcedureName = "[dbo].[BuscarBoletasPassivo]";
            ParameterList.Add(new SqlParameter("@ID", SqlDbType.Int));
            ParameterList.Add(new SqlParameter("@Fundo", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@Distribuidor", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@Cotista", SqlDbType.Int));
            ParameterList.Add(new SqlParameter("@Operação", SqlDbType.Int));
            ParameterList.Add(new SqlParameter("@Referência", SqlDbType.Int));
            ParameterList.Add(new SqlParameter("@De", SqlDbType.Date));
            ParameterList.Add(new SqlParameter("@Até", SqlDbType.Date));
            ParameterList.Add(new SqlParameter("@Aprovadas", SqlDbType.Bit));
            ParameterList.Add(new SqlParameter("@Reprovadas", SqlDbType.Bit));
            ParameterList.Add(new SqlParameter("@Validadas", SqlDbType.Bit));
            ParameterList.Add(new SqlParameter("@EmEspera", SqlDbType.Bit));
        }

        public override void InsertParameterValues()
        {
            if (Cotista == null && Cotistas == null)
            {
                CotistasProvider cotistas = new CotistasProvider();
                cotistas.RunSearch();
                Cotistas = cotistas.Collection;
            }

            if (ID.HasValue) ParameterList.Single(x => x.ParameterName == "@ID").Value = ID; else ParameterList.Single(x => x.ParameterName == "@ID").Value = null;
            ParameterList.Single(x => x.ParameterName == "@Fundo").Value = Fundo.Código;
            if (Distribuidor != null) ParameterList.Single(x => x.ParameterName == "@Distribuidor").Value = Distribuidor.Código; else ParameterList.Single(x => x.ParameterName == "@Distribuidor").Value = null;
            if (Cotista != null) ParameterList.Single(x => x.ParameterName == "@Cotista").Value = Cotista.ID; else ParameterList.Single(x => x.ParameterName == "@Cotista").Value = null;
            if (Operação.HasValue) ParameterList.Single(x => x.ParameterName == "@Operação").Value = (int)Operação; else ParameterList.Single(x => x.ParameterName == "@Operação").Value = null;
            ParameterList.Single(x => x.ParameterName == "@Referência").Value = (int)Referência;
            if (De.HasValue) ParameterList.Single(x => x.ParameterName == "@De").Value = De.Value; else ParameterList.Single(x => x.ParameterName == "@De").Value = null;
            if (Até.HasValue) ParameterList.Single(x => x.ParameterName == "@Até").Value = Até.Value; else ParameterList.Single(x => x.ParameterName == "@Até").Value = null;
            ParameterList.Single(x => x.ParameterName == "@Aprovadas").Value = Convert.ToInt32(Aprovadas);
            ParameterList.Single(x => x.ParameterName == "@Reprovadas").Value = Convert.ToInt32(Reprovadas);
            ParameterList.Single(x => x.ParameterName == "@Validadas").Value = Convert.ToInt32(Validadas);
            ParameterList.Single(x => x.ParameterName == "@EmEspera").Value = Convert.ToInt32(EmEspera);
        }

        public override void GetReferenceTypes(Boleta obj, SqlDataReader reader)
        {
            PropertyInfo property;
            SqlMapper[] attributes;
            string foreignkey;

            property = obj.GetType().GetProperty(nameof(Fundo));
            attributes = property.GetCustomAttributes<SqlMapper>(true).ToArray();
            foreignkey = reader[attributes[0].ColumnName].ToString();
            if (Fundo != null) obj.Fundo = Fundo;
            else obj.Fundo = Fundos.Single(x => x.Código.ToString() == foreignkey);

            property = obj.GetType().GetProperty(nameof(Cotista));
            attributes = property.GetCustomAttributes<SqlMapper>(true).ToArray();
            foreignkey = reader[attributes[0].ColumnName].ToString();
            if (Cotista != null) obj.Cotista = Cotista;
            else obj.Cotista = Cotistas.Single(x => x.ID.ToString() == foreignkey);
        }

        #endregion

    }

    /// <summary>
    /// Fornece métodos de manipulação de dados do tipo <see cref="Boleta"/>.
    /// </summary>
    public class BoletasServicer : DataServicer<Boleta>
    {
        #region Validação

        public override void Validate()
        {
            ValidationErrors.Clear();

            bool isInputValid = true;

            #region Validações de input

            if (!DataAgendada.IsWorkDay())
            { ValidationErrors.Add(nameof(DataAgendada), "A data não é um dia útil."); isInputValid = false; }
            if (Fundo == null)
            { ValidationErrors.Add(nameof(Fundo), "É preciso escolher um fundo."); isInputValid = false; }
            if (Operação == null)
            { ValidationErrors.Add(nameof(Operação), "É preciso escolher uma operação."); isInputValid = false; }
            if (Valor == null && Operação != Gestora.Fundos.Passivo.Operação.ResgateTotal)
            { ValidationErrors.Add(nameof(Valor), "Apenas resgates totais não requerem valor."); isInputValid = false; }
            if (Cotista == null)
            { ValidationErrors.Add(nameof(Cotista), "É preciso escolher um cotista."); isInputValid = false; }
            if (FormaLiquidação == null)
            { ValidationErrors.Add(nameof(FormaLiquidação), "É preciso escolher uma forma de liquidação."); isInputValid = false; }

            #endregion

            #region Validações lógicas

            /*Possíveis erros previstos:

            [Erro]: Resgate total [testado]
            [Erro]: Resgate parcial [testado]
            [Erro]: Aplição menor que mínima inicial [testado]
            [Erro]: Aplicação > Aplicação menor que mínima recorrente [testado]
            [Erro]: Aplicação > Resgate total pedido   > Resgate total pedido [testado]
            [Erro]: Aplicação > Resgate total pedido   > Resgate parcial pedido [testado] 
            [Erro]: Aplicação > Resgate total pedido   > Aplicação menor que mínima inicial [testado] 
            [Erro]: Aplicação > Resgate total pedido   > Aplicação > Aplicação menor que mínima recorrente [testado] 
            [Erro]: Aplicação > Resgate total pedido   > Aplicação > Resgate total pedido [testado] 
            [Erro]: Aplicação > Resgate total cotizado > Resgate total pedido [testado] 
            [Erro]: Aplicação > Resgate total cotizado > Resgate parcial pedido [testado]
            [Erro]: Aplicação > Resgate total cotizado > Aplicação menor que mínima inicial [testado]
            [Erro]: Aplicação > Resgate total cotizado > Aplicação > Aplicação menor que mínima recorrente [testado]
            [Erro]: Aplicação > Resgate parcial menor que resgate mínimo [testado]
            [Erro]: Aplicação > Posição após resgate parcial menor que posição mínima [testado]
            [Erro]: Aplicação > Resgate parcial pedido > Posição após resgate parcial menor que posição mínima [testado]
            [Erro]: Aplicação > Resgate parcial maior que posição [testado]
            [Erro]: Qualquer operação em data com cota validada [testado]*/

            /*Possíveis sucessos previstos:
            [Sucesso]: Aplicação maior que mínima inicial [testado]
            [Sucesso]: Aplicação > Aplicação maior que mínima recorrente [testado]
            [Sucesso]: Aplicação > Resgate total [testado]
            [Sucesso]: Aplicação > Resgate parcial maior que resgate mínimo [testado]
            [Sucesso]: Aplicação > Resgate parcial menor que posição [testado]
            [Sucesso]: Aplicação > Posição após resgate parcial maior que posição mínima [testado]
            [Sucesso]: Aplicação > Resgate parcial > Resgate total [testado]
            [Sucesso]: Aplicação > Resgate total pedido   > Aplicação maior que mínima inicial [testado]
            [Sucesso]: Aplicação > Resgate total pedido   > Aplicação > Aplicação maior que mínima recorrente [testado]
            [Sucesso]: Aplicação > Resgate total pedido   > Aplicação > Resgate total pedido [testado]
            [Sucesso]: Aplicação > Resgate total cotizado > Aplicação maior que mínima inicial [testado]
            [Sucesso]: Aplicação > Resgate total cotizado > Aplicação > Aplicação maior que mínima recorrente [testado]
            [Sucesso]: Aplicação > Resgate total cotizado > Aplicação > Resgate total pedido [testado]*/

            if (isInputValid)
            {
                #region Cotização

                if (Cota.Data == DataAgendada && Cota.Status == Gestora.Fundos.Status.Validado)
                {
                    ValidationErrors.Add(nameof(DataAgendada), $"A cota desta data já está fechada.");
                }

                #endregion

                #region Aplicações

                if (Operação == Gestora.Fundos.Passivo.Operação.Aplicação)
                {
                    BoletasProvider boletas = new BoletasProvider { ID = ID, Cotista = Cotista, Fundo = Fundo, De = null, Até = DataAgendada, Referência = Referência.Agendamento };
                    boletas.RunSearch();
                    Boleta últimoresgatetotal = boletas.Collection.LastOrDefault(x => x.Operação == Gestora.Fundos.Passivo.Operação.ResgateTotal);
                    Boleta últimaaplicação = boletas.Collection.LastOrDefault(x => x.Operação == Gestora.Fundos.Passivo.Operação.Aplicação);

                    if (últimoresgatetotal != null)
                    {
                        if (últimaaplicação == null) throw new ArgumentNullException("Houve um resgate sem haver uma aplicação.");

                        if (últimoresgatetotal.DataCotização > últimaaplicação.DataCotização && últimoresgatetotal.DataAgendada > últimaaplicação.DataCotização)
                        {
                            //Aplicação mínima inicial
                            if (ValorFinanceiro.Value < Fundo.AplicaçãoMínimaInicial)
                                ValidationErrors.Add(nameof(Valor), $"Valor menor que a aplicação inicial mínima do fundo: {Fundo.AplicaçãoMínimaInicial:N2}");
                        }
                        else
                        {
                            //Aplicação mínima
                            if (ValorFinanceiro.Value < Fundo.AplicaçãoMínima)
                                ValidationErrors.Add(nameof(Valor), $"O valor é menor que a aplicação mínima do fundo: {Fundo.AplicaçãoMínima:N2}");
                        }
                    }
                    else if (últimaaplicação == null)
                    {
                        //Aplicação mínima inicial
                        if (ValorFinanceiro.Value < Fundo.AplicaçãoMínimaInicial)
                            ValidationErrors.Add(nameof(Valor), $"O valor é menor que a aplicação inicial mínima do fundo: {Fundo.AplicaçãoMínimaInicial:N2}");
                    }
                    else
                    {

                        //Aplicação mínima
                        if (ValorFinanceiro.Value < Fundo.AplicaçãoMínima)
                            ValidationErrors.Add(nameof(Valor), $"O valor é menor que a aplicação mínima do fundo: {Fundo.AplicaçãoMínima:N2}");
                    }
                }

                #endregion

                #region Resgates e recolhimentos

                else if (Operação == Gestora.Fundos.Passivo.Operação.ResgateTotal || Operação == Gestora.Fundos.Passivo.Operação.ResgateParcial)
                {
                    BoletasProvider aplicações = new BoletasProvider { ID = ID, Cotista = Cotista, Fundo = Fundo, De = null, Até = DataAgendada.AddDays(-1), Referência = Referência.Agendamento };
                    aplicações.RunSearch();
                    BoletasProvider resgates = new BoletasProvider { ID = ID, Cotista = Cotista, Fundo = Fundo, De = null, Até = DataCotização, Referência = Referência.Cotização };
                    resgates.RunSearch();
                    Boleta últimoresgatetotal = resgates.Collection.LastOrDefault(x => x.Operação == Gestora.Fundos.Passivo.Operação.ResgateTotal);
                    Boleta últimaaplicação = aplicações.Collection.LastOrDefault(x => x.Operação == Gestora.Fundos.Passivo.Operação.Aplicação);

                    if (últimoresgatetotal != null)
                    {
                        if (últimaaplicação == null) throw new ArgumentNullException("Houve um resgate sem haver uma aplicação.");

                        //Posição
                        if (últimoresgatetotal.DataCotização > últimaaplicação.DataCotização && últimoresgatetotal.DataAgendada > últimaaplicação.DataCotização)
                        {
                            ValidationErrors.Add(nameof(Cotista), "O cotista já realizou resgate total.");
                        }
                    }
                    else if (últimaaplicação == null)
                    {
                        //Posição
                        ValidationErrors.Add(nameof(Cotista), "O cotista não tem posição no fundo.");
                    }

                    if (Operação == Gestora.Fundos.Passivo.Operação.ResgateParcial && !ValidationErrors.ContainsKey(nameof(Cotista)))
                    {
                        BoletasProvider boletas = new BoletasProvider { ID = ID, Cotista = Cotista, Fundo = Fundo, De = null, Até = DataCotização, Referência = Referência.Cotização };
                        boletas.RunSearch();
                        double saldoEmCotas = 0;
                        
                        foreach (Boleta boleta in boletas.Collection) 
                        {
                            if (boleta.Cotas.HasValue) saldoEmCotas += boleta.Cotas.Value;
                            else saldoEmCotas += boleta.ValorFinanceiro.Value / Cota.Cotação;
                        }

                        if (TipoValor == TipoValor.EmMoeda)
                        {
                            //Resgate mínimo
                            if (Math.Abs(ValorFinanceiro.Value) < Fundo.ResgateMínimo)
                                ValidationErrors.Add(nameof(Valor), $"O valor é menor que o resgate mínimo do fundo: {Fundo.ResgateMínimo:N2}");
                            //Saldo
                            else if (Math.Abs(ValorFinanceiro.Value) >= saldoEmCotas * Cota.Cotação)
                                ValidationErrors.Add(nameof(Valor), "O valor é maior que a posição do cotista.");
                            //Saldo mínimo
                            else if ((saldoEmCotas * Cota.Cotação - Math.Abs(ValorFinanceiro.Value)) < Fundo.SaldoMínimo)
                                ValidationErrors.Add(nameof(Valor), "A posição após o resgate seria menor que o saldo mínimo de permanência.");
                        }
                        else
                        {
                            //Resgate mínimo
                            if (Math.Abs(Cotas.Value) < Fundo.ResgateMínimo / Cota.Cotação)
                                ValidationErrors.Add(nameof(Valor), $"O valor convertido é menor que o resgate mínimo do fundo: {Fundo.ResgateMínimo:N2}");
                            //Saldo
                            else if (Math.Abs(Cotas.Value) >= saldoEmCotas)
                                ValidationErrors.Add(nameof(Valor), "O valor é maior que a posição do cotista.");
                            //Saldo mínimo
                            else if ((saldoEmCotas - Math.Abs(Cotas.Value)) < Fundo.SaldoMínimo / Cota.Cotação)
                                ValidationErrors.Add(nameof(Valor), "A posição após o resgate seria menor que o saldo mínimo de permanência.");
                        }
                    }
                }
                #endregion
            }

            #endregion

            OnPropertyChanged(null);
        }

        #endregion

        #region Campos

        private DateTime dataagendada;
        private DateTime? dataregistro;
        private Fundo fundo;
        private Cota cota;
        private Operação? operação;
        private double? valor;
        private TipoValor tipovalor;
        private Cotista cotista;
        private FormaLiquidação? formaliquidação;
        private string comentários;
        private Status status;
        private FormaImportação formaimportação;

        #endregion

        #region Propriedades

        public int? ID { get; set; }
        public ObservableCollection<Fundo> Fundos { get; set; }
        private Cota Cota 
        { 
            get
            {
                if (cota == null)
                {
                    CotasProvider cotas = new CotasProvider { Fundo = Fundo, De = DataAgendada, Até = DataAgendada };
                    cotas.RunSearch();
                    cota = cotas.Collection[0];
                }
                if (cota.Data != DataAgendada) 
                {
                    CotasProvider cotas = new CotasProvider { Fundo = Fundo, De = DataAgendada, Até = DataAgendada };
                    cotas.RunSearch();
                    cota = cotas.Collection[0];
                }
                return cota;
            } 
        }

        public DateTime DataCotização
        {
            get
            {
                if (Operação == Gestora.Fundos.Passivo.Operação.Aplicação)
                {
                    DateTime data = DataAgendada.AddDays(Fundo.ConversãoAplicação);
                    if (data.IsWorkDay()) return data;
                    else return data.GetNextWorkDay();
                }
                else if (Operação == Gestora.Fundos.Passivo.Operação.RecolhimentoImpostos)
                {
                    return DataAgendada;
                }
                else
                {
                    DateTime data = DataAgendada.AddDays(Fundo.ConversãoResgate);
                    if (data.IsWorkDay()) return data;
                    else return data.GetNextWorkDay();
                }
            }
        }
        public DateTime DataLiquidação
        {
            get
            {
                if (Operação == Gestora.Fundos.Passivo.Operação.Aplicação)
                {
                    DateTime data = DataCotização.AddDays(Fundo.LiquidaçãoAplicação);
                    if (data.IsWorkDay()) return data;
                    else return data.GetNextWorkDay();
                }
                else if (Operação == Gestora.Fundos.Passivo.Operação.RecolhimentoImpostos)
                {
                    DateTime data = DataCotização.AddDays(1);
                    if (data.IsWorkDay()) return data;
                    else return data.GetNextWorkDay();
                }
                else
                {
                    DateTime data = DataCotização.AddDays(Fundo.LiquidaçãoResgate);
                    if (data.IsWorkDay()) return data;
                    else return data.GetNextWorkDay();
                }
            }
        }
        public DateTime DataAgendada { get { return dataagendada; } set { dataagendada = value; OnPropertyChanged(); } }
        public DateTime? DataRegistro { get { return dataregistro; } set { dataregistro = value; OnPropertyChanged(); } }
        public Fundo Fundo { get { return fundo; } set { fundo = value; OnPropertyChanged(); } }
        public Operação? Operação
        {
            get { return operação; }
            set
            {
                if (value == Gestora.Fundos.Passivo.Operação.ResgateTotal)
                {
                    TipoValor = TipoValor.EmCotas;
                    Valor = null;
                }
                else
                {
                    TipoValor = TipoValor.EmMoeda;
                    Valor = null;
                }

                operação = value;
                OnPropertyChanged();
            }
        }
        public double? Valor { get { return valor; } set { valor = value; OnPropertyChanged(); } }
        public double? ValorFinanceiro 
        { 
            get 
            { 
                if (Operação == Gestora.Fundos.Passivo.Operação.Aplicação)
                    return TipoValor == TipoValor.EmMoeda && Valor.HasValue ? Math.Round(Valor.Value, 2) : (double?)null; 
                else
                    return TipoValor == TipoValor.EmMoeda && Valor.HasValue ? Math.Round(-Valor.Value, 2) : (double?)null;
            } 
        }
        public double? Cotas 
        { 
            get 
            {
                if (Operação == Gestora.Fundos.Passivo.Operação.Aplicação)
                    return TipoValor == TipoValor.EmCotas && Valor.HasValue ? Math.Round(Valor.Value, 8) : (double?)null;
                else
                    return TipoValor == TipoValor.EmCotas && Valor.HasValue ? Math.Round(-Valor.Value, 8) : (double?)null;
            } 
        }
        public Cotista Cotista { get { return cotista; } set { cotista = value; OnPropertyChanged(); } }
        public FormaLiquidação? FormaLiquidação { get { return formaliquidação; } set { formaliquidação = value; OnPropertyChanged(); } }
        public string Comentários { get { return comentários; } set { comentários = value; OnPropertyChanged(); } }
        public TipoValor TipoValor { get { return tipovalor; } set { tipovalor = value; OnPropertyChanged(); } }
        public Status Status { get { return status; } set { status = value; OnPropertyChanged(); } }
        public FormaImportação FormaImportação { get { return formaimportação; } set { formaimportação = value; OnPropertyChanged(); } }

        #endregion

        #region Métodos

        public BoletasServicer()
        {
            ManipulationType = Dados.ManipulationType.Insert;
            DataAgendada = DateTime.Today;
            Status = Status.EmEspera;
        }

        protected override void AddProcedureParameters()
        {
            InsertProcedureName = "[dbo].[InserirBoletaPassivo]";
            UpdateProcedureName = "[dbo].[AlterarBoletaPassivo]";
            ParameterList.Add(new SqlParameter("@ID", SqlDbType.Int));
            ParameterList.Add(new SqlParameter("@DataCotização", SqlDbType.Date));
            ParameterList.Add(new SqlParameter("@DataLiquidação", SqlDbType.Date));
            ParameterList.Add(new SqlParameter("@DataAgendada", SqlDbType.Date));
            ParameterList.Add(new SqlParameter("@DataRegistro", SqlDbType.DateTime));
            ParameterList.Add(new SqlParameter("@Fundo", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@Operação", SqlDbType.SmallInt));
            ParameterList.Add(new SqlParameter("@ValorFinanceiro", SqlDbType.Money));
            ParameterList.Add(new SqlParameter("@Cotas", SqlDbType.Float));
            ParameterList.Add(new SqlParameter("@Cotista", SqlDbType.Int));
            ParameterList.Add(new SqlParameter("@FormaLiquidação", SqlDbType.SmallInt));
            ParameterList.Add(new SqlParameter("@Comentários", SqlDbType.VarChar, 400));
            ParameterList.Add(new SqlParameter("@TipoValor", SqlDbType.SmallInt));
            ParameterList.Add(new SqlParameter("@Status", SqlDbType.SmallInt));
            ParameterList.Add(new SqlParameter("@FormaImportação", SqlDbType.SmallInt));
        }

        protected override void InsertParameterValues()
        {
            ParameterList.Single(x => x.ParameterName == "@ID").Value = ID;
            ParameterList.Single(x => x.ParameterName == "@DataCotização").Value = DataCotização;
            ParameterList.Single(x => x.ParameterName == "@DataLiquidação").Value = DataLiquidação;
            ParameterList.Single(x => x.ParameterName == "@DataAgendada").Value = DataAgendada;
            if (DataRegistro.HasValue) ParameterList.Single(x => x.ParameterName == "@DataRegistro").Value = DataRegistro; 
            else ParameterList.Single(x => x.ParameterName == "@DataRegistro").Value = DateTime.Now;
            ParameterList.Single(x => x.ParameterName == "@Fundo").Value = Fundo.Código;
            ParameterList.Single(x => x.ParameterName == "@Operação").Value = (int)Operação;
            ParameterList.Single(x => x.ParameterName == "@ValorFinanceiro").Value = ValorFinanceiro ?? null;
            ParameterList.Single(x => x.ParameterName == "@Cotas").Value = Cotas ?? null;
            ParameterList.Single(x => x.ParameterName == "@Cotista").Value = Cotista.ID;
            ParameterList.Single(x => x.ParameterName == "@FormaLiquidação").Value = (int)FormaLiquidação;
            ParameterList.Single(x => x.ParameterName == "@Comentários").Value = Comentários == string.Empty ? null : Comentários;
            ParameterList.Single(x => x.ParameterName == "@TipoValor").Value = (int)TipoValor;
            ParameterList.Single(x => x.ParameterName == "@Status").Value = (int)Status;
            ParameterList.Single(x => x.ParameterName == "@FormaImportação").Value = (int)FormaImportação;
        }

        #endregion
    }
}
