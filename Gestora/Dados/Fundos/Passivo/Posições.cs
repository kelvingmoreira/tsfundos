﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;
using Gestora.Dados;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlTypes;
using Gestora.Fundos;
using Gestora.Fundos.Passivo;
using Gestora.Helpers;
using System.Collections.Generic;
using System.Threading.Tasks;
using Gestora.Dados.Fundos.Predicados;

namespace Gestora.Dados.Fundos.Passivo
{
    /// <summary>
    /// Fornece métodos de provisão de dados do tipo <see cref="Gestora.Fundos.Passivo.Posição"/>.
    /// </summary>
    /// <remarks>
    /// Obs.: O construtor <see cref="PosiçõesProvider()"/> não cria a coleção de dados. 
    /// </remarks>
    public class PosiçõesProvider : DataProvider<Posição>, IDataErrorInfo
    {
        #region IDataErrorInfo Members

        public string Error
        {
            get
            {
                if (ValidationErrors.Count > 0)
                {
                    return $"{ValidationErrors.Count} erro{(ValidationErrors.Count == 1 ? "" : "s")} encontrado{(ValidationErrors.Count == 1 ? "" : "s")}";
                }
                return null;
            }
        }

        public string this[string columnName]
        {
            get
            {
                if (ValidationErrors.ContainsKey(columnName))
                {
                    return ValidationErrors[columnName];
                }
                return null;
            }
        }

        #endregion

        #region Validação

        protected Dictionary<string, string> ValidationErrors = new Dictionary<string, string>();

        protected void Validate()
        {
            ValidationErrors.Clear();

            bool isInputValid = true;

            #region Validações de input

            if (Fundo == null)
            { ValidationErrors.Add(nameof(Fundo), "É preciso escolher um fundo."); isInputValid = false; }

            if (Posição == null)
            { ValidationErrors.Add(nameof(Posição), "É preciso escolher uma data."); isInputValid = false; }
            else if (!Posição.Value.IsWorkDay())
            { ValidationErrors.Add(nameof(Posição), "A data não é um dia útil."); isInputValid = false; }

            #endregion

            #region Validações lógicas

            if (isInputValid)
            {
                if (Cotas.Collection.FirstOrDefault(x => x.Data == Posição) == null)
                { ValidationErrors.Add(nameof(Posição), "A data não possui valor de cota validado."); isInputValid = false; }
                else if (Cotas.Collection.FirstOrDefault(x => x.Data == Posição).Status == Gestora.Fundos.Status.EmAberto)
                { ValidationErrors.Add(nameof(Posição), "A data não possui valor de cota validado."); isInputValid = false; }
            }

            #endregion

            OnPropertyChanged(null);
        }

        #endregion

        #region Campos

        private Fundo fundo;
        private Distribuidor distribuidor;
        private DateTime? posição;
        private CotasProvider cotas;

        #endregion

        #region Propriedades

        public ObservableCollection<Fundo> Fundos { get; set; }
        public ObservableCollection<Cotista> Cotistas { get; set; }
        public Fundo Fundo 
        { 
            get { return fundo; } 
            set 
            {
                fundo = value;
                Cotas = new CotasProvider { Fundo = Fundo };
                Cotas.RunSearch();
                OnPropertyChanged();
                OnPropertyChanged(nameof(Cotas));
            } 
        }
        public Distribuidor Distribuidor { get { return distribuidor; } set { distribuidor = value; OnPropertyChanged(); } }
        public DateTime? Posição { get { return posição; } set { posição = value; OnPropertyChanged(); } }
        public CotasProvider Cotas { get; set; }

        #endregion

        #region Métodos

        /// <summary>
        /// Inicializa uma nova instância da classe <see cref="PosiçõesProvider"/>
        /// </summary>
        public PosiçõesProvider()
        {
            Fundos = new Fundos().Collection;
            CotistasProvider cotistas = new CotistasProvider();
            cotistas.RunSearch();
            Cotistas = cotistas.Collection;
        }

        public override void AddProcedureParameters()
        {
            ProcedureName = "[dbo].[BuscarPosições]";
            ParameterList.Add(new SqlParameter("@Fundo", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@Distribuidor", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@Posição", SqlDbType.Date));
        }

        public override void InsertParameterValues()
        {
            ParameterList.Single(x => x.ParameterName == "@Fundo").Value = Fundo.Código;
            ParameterList.Single(x => x.ParameterName == "@Posição").Value = Posição;
            if (Distribuidor != null) ParameterList.Single(x => x.ParameterName == "@Distribuidor").Value = Distribuidor.Código;
            else ParameterList.Single(x => x.ParameterName == "@Distribuidor").Value = null;
        }

        public override void GetReferenceTypes(Posição obj, SqlDataReader reader)
        {
            PropertyInfo property;
            SqlMapper[] attributes;
            string foreignkey;

            obj.Fundo = Fundo;

            property = obj.GetType().GetProperty(nameof(Cotista));
            attributes = property.GetCustomAttributes<SqlMapper>(true).ToArray();
            foreignkey = reader[attributes[0].ColumnName].ToString();
            obj.Cotista = Cotistas.Single(x => x.ID.ToString() == foreignkey);
        }

        #endregion
    }
}
