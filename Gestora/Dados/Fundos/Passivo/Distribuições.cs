﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Collections.ObjectModel;
using Gestora.Dados.Primitivos;
using Gestora.Fundos;
using Gestora.Fundos.Passivo;


namespace Gestora.Dados.Fundos.Passivo
{
    /// <summary>
    /// Fornece métodos de relação many-to-many entre <see cref="Distribuidor"/> e <see cref="Fundo"/>.
    /// </summary>
    /// <remarks>
    /// Obs.: O construtor <see cref="Distribuições(Fundo)"/> cria a coleção de dados. 
    /// </remarks>
    public class Distribuições : DataProvider<Distribuidor>
    {
        private ContasBancáriasProvider ContasBancárias { get; set; }
        private Fundo Fundo { get; set; }

        /// <summary>
        /// Inicializa uma nova instância da classe <see cref="Distribuições"/>
        /// </summary>
        /// <param name="fundo">O <see cref="Fundo"/> ao qual se deseja atribuir os <see cref="Fundo.Distribuidores"/> </param>
        /// <remarks>
        /// O método <see cref="DataProvider{T}.RunSearch"/> é utilizado.
        /// </remarks>
        public Distribuições(Fundo fundo)
        {
            ContasBancárias = new ContasBancáriasProvider();
            Fundo = fundo;
            RunSearch();
        }

        public override void AddProcedureParameters()
        {
            ProcedureName = "[dbo].[BuscarDistribuições]";
            ParameterList.Add(new SqlParameter("@Fundo", SqlDbType.VarChar, 100));
        }

        public override void InsertParameterValues()
        {
            if (Fundo != null) ParameterList.Single(x => x.ParameterName == "@Fundo").Value = Fundo.Código;
        }

        public override void GetReferenceTypes(Distribuidor obj, SqlDataReader reader)
        {
            PropertyInfo property;
            SqlMapper[] attributes;
            string foreignkey;

            property = obj.GetType().GetProperty(nameof(Distribuidor.ContaResgate));
            attributes = property.GetCustomAttributes<SqlMapper>(true).ToArray();
            foreignkey = reader[attributes[0].ColumnName].ToString();

            obj.ContaResgate = ContasBancárias.Collection.FirstOrDefault(x => x.ID.ToString() == foreignkey);

            property = obj.GetType().GetProperty(nameof(Distribuidor.ContaAplicação));
            attributes = property.GetCustomAttributes<SqlMapper>(true).ToArray();
            foreignkey = reader[attributes[0].ColumnName].ToString();

            obj.ContaAplicação = ContasBancárias.Collection.FirstOrDefault(x => x.ID.ToString() == foreignkey);

            property = obj.GetType().GetProperty(nameof(Distribuidor.ContaRebate));
            attributes = property.GetCustomAttributes<SqlMapper>(true).ToArray();
            foreignkey = reader[attributes[0].ColumnName].ToString();

            obj.ContaRebate = ContasBancárias.Collection.FirstOrDefault(x => x.ID.ToString() == foreignkey);
        }
    }
}
