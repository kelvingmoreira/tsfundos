﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Reflection;
using Gestora.Dados.Primitivos;
using Gestora.Fundos;
using Gestora.Fundos.Passivo;

namespace Gestora.Dados.Fundos.Passivo
{
    /// <summary>
    /// Fornece métodos de provisão de dados do tipo <see cref="Distribuidor"/>.
    /// </summary>
    /// <remarks>
    /// Obs.: O construtor <see cref="Distribuidores()"/> cria a coleção de dados. 
    /// </remarks>
    public class Distribuidores : DataProvider<Distribuidor>
    {
        private ContasBancáriasProvider ContasBancárias { get; set; }

        /// <summary>
        /// Inicializa uma nova instância da classe <see cref="Distribuidores"/>
        /// </summary>
        /// <remarks>
        /// O método <see cref="DataProvider{T}.RunSearch"/> é utilizado.
        /// </remarks>
        public Distribuidores()
        {
            ContasBancárias = new ContasBancáriasProvider();
            RunSearch();
        }

        public override void AddProcedureParameters()
        {
            ProcedureName = "[dbo].[BuscarDistribuidores]";
            ParameterList.Add(new SqlParameter("@Código", SqlDbType.VarChar, 100));
        }

        public override void GetReferenceTypes(Distribuidor obj, SqlDataReader reader)
        {
            PropertyInfo property;
            SqlMapper[] attributes;
            string foreignkey;

            property = obj.GetType().GetProperty(nameof(Distribuidor.ContaResgate));
            attributes = property.GetCustomAttributes<SqlMapper>(true).ToArray();
            foreignkey = reader[attributes[0].ColumnName].ToString();

            obj.ContaResgate = ContasBancárias.Collection.FirstOrDefault(x => x.ID.ToString() == foreignkey);

            property = obj.GetType().GetProperty(nameof(Distribuidor.ContaAplicação));
            attributes = property.GetCustomAttributes<SqlMapper>(true).ToArray();
            foreignkey = reader[attributes[0].ColumnName].ToString();

            obj.ContaAplicação = ContasBancárias.Collection.FirstOrDefault(x => x.ID.ToString() == foreignkey);

            property = obj.GetType().GetProperty(nameof(Distribuidor.ContaRebate));
            attributes = property.GetCustomAttributes<SqlMapper>(true).ToArray();
            foreignkey = reader[attributes[0].ColumnName].ToString();

            obj.ContaRebate = ContasBancárias.Collection.FirstOrDefault(x => x.ID.ToString() == foreignkey);
        }
    }
}
