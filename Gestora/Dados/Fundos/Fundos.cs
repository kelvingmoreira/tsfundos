﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Reflection;
using Gestora.Dados.Primitivos;
using Gestora.Fundos;
using Gestora.Primitivos;
using Gestora.Dados.Fundos.Passivo;

namespace Gestora.Dados.Fundos
{
    /// <summary>
    /// Fornece métodos de provisão de dados do tipo <see cref="Fundo"/>.
    /// </summary>
    /// <remarks>
    /// Obs.: O construtor <see cref="Fundos()"/> cria a coleção de dados. 
    /// </remarks>
    public class Fundos : DataProvider<Fundo>
    {
        #region Propriedades

        private Administradores Administradores { get; set; }
        private Custodiantes Custodiantes { get; set; }

        #endregion

        #region Métodos

        /// <summary>
        /// Inicializa uma nova instância da classe <see cref="Fundos"/>
        /// </summary>
        /// <remarks>
        /// O método <see cref="DataProvider{T}.RunSearch"/> é utilizado.
        /// </remarks>
        public Fundos()
        {
            Administradores = new Administradores();
            Custodiantes = new Custodiantes();
            RunSearch();
        }

        public override void AddProcedureParameters()
        {
            ProcedureName = "[dbo].[BuscarFundos]";
            ParameterList.Add(new SqlParameter("@Fundo", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@Administrador", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@Custodiante", SqlDbType.VarChar, 100));
        }

        public override void GetReferenceTypes(Fundo obj, SqlDataReader reader)
        {
            PropertyInfo property;
            SqlMapper[] attributes;
            string foreignkey;

            property = obj.GetType().GetProperty(nameof(Administrador));
            attributes = property.GetCustomAttributes<SqlMapper>(true).ToArray();
            foreignkey = reader[attributes[0].ColumnName].ToString();

            obj.Administrador = Administradores.Collection.Single(x => x.Código == foreignkey);

            property = obj.GetType().GetProperty(nameof(Custodiante));
            attributes = property.GetCustomAttributes<SqlMapper>(true).ToArray();
            foreignkey = reader[attributes[0].ColumnName].ToString();

            obj.Custodiante = Custodiantes.Collection.Single(x => x.Código == foreignkey);

            obj.Distribuidores = new Distribuições(obj).Collection;
        }

        #endregion
    }
}
