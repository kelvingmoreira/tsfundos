﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Reflection;
using Gestora.Dados;
using Gestora.Primitivos;
using System.Threading.Tasks;

namespace Gestora.Dados.Primitivos
{
    /// <summary>
    /// Fornece métodos de provisão de dados do tipo <see cref="Custodiante"/>.
    /// </summary>
    /// <remarks>
    /// Obs.: O construtor <see cref="Custodiantes()"/> cria a coleção de dados. 
    /// </remarks>
    public class Custodiantes : DataProvider<Custodiante>
    {
        /// <summary>
        /// Inicializa uma nova instância da classe <see cref="Administradores"/>
        /// </summary>
        /// <remarks>
        /// O método <see cref="DataProvider{T}.RunSearch"/> é utilizado.
        /// </remarks>
        public Custodiantes()
        {
            RunSearch();
        }

        public override void AddProcedureParameters()
        {
            ProcedureName = "[dbo].[BuscarCustodiantes]";
        }

        public override void GetReferenceTypes(Custodiante obj, SqlDataReader reader) {}
    }
}
