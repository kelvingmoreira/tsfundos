﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Data.SqlClient;
using Gestora.Primitivos;
using System.Threading.Tasks;

namespace Gestora.Dados.Primitivos
{
    /// <summary>
    /// Fornece métodos de provisão de dados do tipo <see cref="Administrador"/>.
    /// </summary>
    /// <remarks>
    /// Obs.: O construtor <see cref="Administradores()"/> cria a coleção de dados. 
    /// </remarks>
    public class Administradores : DataProvider<Administrador>
    {
        /// <summary>
        /// Inicializa uma nova instância da classe <see cref="Administradores"/>
        /// </summary>
        /// <remarks>
        /// O método <see cref="DataProvider{T}.RunSearch"/> é utilizado.
        /// </remarks>
        public Administradores()
        {
            RunSearch();
        }

        public override void AddProcedureParameters()
        {
            ProcedureName = "[dbo].[BuscarAdministradores]";
        }

        public override void GetReferenceTypes(Administrador obj, SqlDataReader reader) {}
    }
}
