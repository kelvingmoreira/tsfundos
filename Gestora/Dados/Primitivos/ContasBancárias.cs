﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Data.SqlClient;
using Gestora.Primitivos;
using System.Threading.Tasks;
using System.Data;
using System.Linq;

namespace Gestora.Dados.Primitivos
{
    /// <summary>
    /// Fornece métodos de provisão de dados do tipo <see cref="ContaBancária"/>.
    /// </summary>
    /// <remarks>
    /// Obs.: O construtor <see cref="ContasBancáriasProvider()"/> cria a coleção de dados. 
    /// </remarks>
    public class ContasBancáriasProvider : DataProvider<ContaBancária>
    {
        /// <summary>
        /// Inicializa uma nova instância da classe <see cref="ContasBancáriasProvider"/>
        /// </summary>
        /// <remarks>
        /// O método <see cref="DataProvider{T}.RunSearch"/> é utilizado.
        /// </remarks>
        public ContasBancáriasProvider()
        {
            RunSearch();
        }

        public override void AddProcedureParameters()
        {
            ProcedureName = "[dbo].[BuscarContasBancárias]";
        }

        public override void GetReferenceTypes(ContaBancária obj, SqlDataReader reader) { }
    }

    public class ContasBancáriasServicer : DataServicer<ContaBancária>
    {
        #region Campos

        private int? id;
        private string nome;
        private string banco;
        private string agência;
        private string conta;
        private string dígito;

        #endregion

        #region Propriedades

        public int? ID { get { return id; } set { id = value; OnPropertyChanged(); } }
        public string Nome { get { return nome; } set { nome = value; OnPropertyChanged(); } }
        public string Banco { get { return banco; } set { banco = value; OnPropertyChanged(); } }
        public string Agência { get { return agência; } set { agência = value; OnPropertyChanged(); } }
        public string Conta { get { return conta; } set { conta = value; OnPropertyChanged(); } }
        public string Dígito { get { return dígito; } set { dígito = value; OnPropertyChanged(); } }

        #endregion

        public ContasBancáriasServicer()
        {
            ManipulationType = Dados.ManipulationType.Insert;
        }

        public override void Validate()
        {
            if (string.IsNullOrWhiteSpace(Banco) || string.IsNullOrWhiteSpace(Agência) || string.IsNullOrWhiteSpace(Conta) || string.IsNullOrWhiteSpace(Dígito))
                ValidationErrors.Add(nameof(Banco), "É preciso incluir os dados bancários.");
            else
            {
                ContaBancária cb = new ContasBancáriasProvider().Collection.SingleOrDefault(x => (x.Banco == Banco && x.Agência == Agência && x.Conta == Conta && x.Dígito == Dígito));
                if (cb != null && (ID == null || ID != cb.ID)) ValidationErrors.Add(nameof(Banco), "A conta bancária já possui associado.");
            }
            OnPropertyChanged(null);
        }

        protected override void AddProcedureParameters()
        {
            InsertProcedureName = "[dbo].[InserirContaBancária]";
            UpdateProcedureName = "[dbo].[AlterarContaBancária]";
            ParameterList.Add(new SqlParameter("@ID", SqlDbType.Int));
            ParameterList.Add(new SqlParameter("@Nome", SqlDbType.VarChar, 100));
            ParameterList.Add(new SqlParameter("@Banco", SqlDbType.VarChar, 3));
            ParameterList.Add(new SqlParameter("@Agência", SqlDbType.VarChar, 12));
            ParameterList.Add(new SqlParameter("@Conta", SqlDbType.VarChar, 12));
            ParameterList.Add(new SqlParameter("@Dígito", SqlDbType.NChar, 1));
        }

        protected override void InsertParameterValues()
        {
            ParameterList.Single(x => x.ParameterName == "@ID").Value = ID;
            ParameterList.Single(x => x.ParameterName == "@Nome").Value = string.IsNullOrWhiteSpace(Nome) ? null : Nome;
            ParameterList.Single(x => x.ParameterName == "@Banco").Value = Banco;
            ParameterList.Single(x => x.ParameterName == "@Agência").Value = Agência;
            ParameterList.Single(x => x.ParameterName == "@Conta").Value = Conta;
            ParameterList.Single(x => x.ParameterName == "@Dígito").Value = Dígito;
        }
    }
}
