﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace TSFundos.Dialogs
{
    /// <summary>
    /// Interação lógica para MessageDialog.xam
    /// </summary>
    public partial class MessageDialog : CustomDialog, IDialogViewModel<MessageDialogResult>, INotifyPropertyChanged
    {
        #region IDialogViewModel members
        private readonly TaskCompletionSource<MessageDialogResult> _tcs;
        public void Close(MessageDialogResult result)
        {
            _tcs.SetResult(result);
            Closed?.Invoke(this, EventArgs.Empty);
        }
        public Task<MessageDialogResult> Task => _tcs.Task;
        public event EventHandler Closed;
        public ICommand ActionCommand { get; set; }
        private void OnDialogCommandExecute(object messageDialogResult)
        {
            Close((MessageDialogResult)messageDialogResult);
        }
        #endregion

        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        public MessageDialogStyle MessageDialogStyle { get; set; }
        public string Título { get; set; }
        public string Mensagem { get; set; }

        public MessageDialog(string título, string mensagem, MessageDialogStyle messageDialogStyle)
        {
            Título = título;
            Mensagem = mensagem;
            MessageDialogStyle = messageDialogStyle;
            InitializeComponent();

            _tcs = new TaskCompletionSource<MessageDialogResult>();
            ActionCommand = new TSFundos.RelayCommand(OnDialogCommandExecute);
            OnPropertyChanged(nameof(ActionCommand));
        }

        private async void CustomDialog_Loaded(object sender, RoutedEventArgs e)
        {
            await System.Threading.Tasks.Task.Delay(200);
            //PrimaryButton.Focus();
        }
    }
}
