﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using MahApps.Metro.Controls.Dialogs;

namespace TSFundos.Dialogs
{
    public class CustomDialogViewModel : DialogViewModel<MessageDialogResult>
    {
        private string _title;
        private string _message;
        private string _affirmativeButtonText = "Yes";
        private string _negativeButtonText = "No";
        private ICommand _dialogCommand;

        public CustomDialogViewModel()
        {
            _dialogCommand = new RelayCommand<MessageDialogResult>(OnDialogCommandExecute, result => true);
        }

        private void OnDialogCommandExecute(MessageDialogResult messageDialogResult)
        {
            Close(messageDialogResult);
        }

        public ICommand DialogCommand
        {
            get { return _dialogCommand; }
        }
    }
}