﻿using MahApps.Metro.Controls.Dialogs;
namespace TSFundos.Dialogs
{
    /// <summary>
    /// Interaction logic for CustomDialogView.xaml
    /// </summary>
    public partial class CustomDialogView : CustomDialog
    {
        public CustomDialogView()
        {
            InitializeComponent();
        }
    }
}