﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Extensions.DependencyInjection;

namespace TSFundos.Dialogs
{
    public class DialogManager
    {
        public async Task<TResult> ShowDialogAsync<TResult>(IDialogViewModel<TResult> viewModel, CustomDialog view,  MetroDialogSettings settings = null)
        {
            if (!(view is BaseMetroDialog dialog))
            {
                throw new InvalidOperationException($"The view {view.GetType()} belonging to view model {viewModel.GetType()} does not inherit from {typeof(BaseMetroDialog)}");
            }

            view.DataContext = viewModel;

            var firstMetroWindow = Application.Current.Windows.OfType<MetroWindow>().First();
            await firstMetroWindow.ShowMetroDialogAsync(dialog, settings);
            var result = await viewModel.Task;
            await firstMetroWindow.HideMetroDialogAsync(dialog, settings);

            return result;
        }

        public async Task<TResult> ShowDialogAsync<TResult>(DialogViewModel<TResult> viewModel, CustomDialog view, MetroDialogSettings settings = null)
        {
            if (!(view is BaseMetroDialog dialog))
            {
                throw new InvalidOperationException($"The view {view.GetType()} belonging to view model {viewModel.GetType()} does not inherit from {typeof(BaseMetroDialog)}");
            }

            view.DataContext = viewModel;

            var firstMetroWindow = Application.Current.Windows.OfType<MetroWindow>().First();
            await firstMetroWindow.ShowMetroDialogAsync(dialog, settings);
            var result = await viewModel.Task;
            await firstMetroWindow.HideMetroDialogAsync(dialog, settings);

            return result;
        }

        public async Task ShowMessageBox(string title, string message, MetroDialogSettings settings = null)
        {
            var firstMetroWindow = Application.Current.Windows.OfType<MetroWindow>().First();
            MessageDialog msgdialog = new MessageDialog(title, message, MessageDialogStyle.Affirmative);
            await firstMetroWindow.ShowMetroDialogAsync(msgdialog, settings);
            var result = await msgdialog.Task;
            await firstMetroWindow.HideMetroDialogAsync(msgdialog, settings);
        }

        public async Task<MessageDialogResult> ShowDefaultDialog(string title, string message, MessageDialogStyle style = MessageDialogStyle.AffirmativeAndNegative, MetroDialogSettings settings = null)
        {
            var firstMetroWindow = Application.Current.Windows.OfType<MetroWindow>().First();
            MessageDialog msgdialog = new MessageDialog(title, message, style);
            await firstMetroWindow.ShowMetroDialogAsync(msgdialog, settings);
            MessageDialogResult result = await msgdialog.Task;
            await firstMetroWindow.HideMetroDialogAsync(msgdialog, settings);

            return result;
        }
    }
}
