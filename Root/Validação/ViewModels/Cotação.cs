﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Gestora.Dados;
using Gestora.Dados.Fundos;
using Gestora.Dados.Fundos.Passivo;
using Gestora.Fundos.Passivo;
using MahApps.Metro.Controls.Dialogs;
using Gestora.Dados.Primitivos;
using TSFundos.Dialogs;
using System.Collections.Generic;
using TSFundos.Helpers;
using Microsoft.Win32;
using System.IO;
using System.Text;

namespace TSFundos.Root.Validação.ViewModels
{
    public class Cotação
    {
        Dialogs.DialogManager dialogManager;

        public Cotação()
        {
            dialogManager = new Dialogs.DialogManager();
            AdicionarCommand = new RelayCommand(Adicionar);
        }

        #region Comandos

        public ICommand AdicionarCommand { get; set; }

        private async void Adicionar(object obj)
        {
            var CotaçãoDialog = new Validação.CotaçãoDialog();
            var CotaçãoDialogVM = new CotaçãoDialog();

            MessageDialogResult result = await dialogManager.ShowDialogAsync(CotaçãoDialogVM, CotaçãoDialog);
            if (result == MessageDialogResult.Affirmative)
            {
                await dialogManager.ShowMessageBox("Aviso", "Cota cadastrada com sucesso.");
            }
        }

        #endregion
    }
}
