﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gestora.Dados;
using Gestora.Dados.Fundos;
using Gestora.Dados.Fundos.Passivo;
using MahApps.Metro.Controls.Dialogs;
using TSFundos.Dialogs;
using System.Windows.Input;
using System.ComponentModel;
using Gestora.Helpers;
using Gestora.Fundos.Passivo;
using Gestora.Fundos;
using Gestora.Dados.Primitivos;
using TSFundos.Converters;
using Gestora.Dados.Fundos.Predicados;

namespace TSFundos.Root.Validação.ViewModels
{
    public class CotaçãoDialog : CotasServicer, IDialogViewModel<MessageDialogResult>
    {
        #region IDialogViewModel members
        private readonly TaskCompletionSource<MessageDialogResult> _tcs;
        public void Close(MessageDialogResult result)
        {
            _tcs.SetResult(result);
            Closed?.Invoke(this, EventArgs.Empty);
        }
        public Task<MessageDialogResult> Task => _tcs.Task;
        public event EventHandler Closed;
        private ICommand _dialogCommand;
        public ICommand DialogCommand { get { return _dialogCommand; } }
        private void OnDialogCommandExecute(MessageDialogResult messageDialogResult)
        {
            if (messageDialogResult == MessageDialogResult.Affirmative)
            {
                Validate();
                if (ValidationErrors.Count == 0)
                {
                    ExecuteNonQuery();
                    Close(messageDialogResult);
                }
            }
            else if (messageDialogResult == MessageDialogResult.Negative)
            {
                Close(messageDialogResult);
            }
        }
        #endregion

        private bool isreadonly;
        private bool isediting;
        public bool IsReadOnly { get { return isreadonly; } set { isreadonly = value; OnPropertyChanged(null); } }
        public bool IsEnabled { get { return !IsReadOnly; } }
        public bool IsEditing { get { return isediting; } set { isediting = value; OnPropertyChanged(); } }
        public string PrimaryButtonContent { get { return IsEditing ? "Alterar" : "Cadastrar"; } }
        public string NegativeButtonContent { get { return IsReadOnly ? "Fechar" : "Cancelar"; } }

        public CotaçãoDialog()
        {
            _tcs = new TaskCompletionSource<MessageDialogResult>();
            _dialogCommand = new RelayCommand<MessageDialogResult>(OnDialogCommandExecute, result => true);
        }
    }
}
