﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using MahApps.Metro.Controls;
using System.ComponentModel;
using MahApps.Metro.Controls.Dialogs;
using System.Runtime.CompilerServices;
using System.Collections.Generic;

namespace TSFundos
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : MetroWindow, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private bool mRestoreForDragMove;
        private Visibility _UserInfo;
        private string _Nome, _Inicial;
        public string Nome { get { return _Nome; } set { _Nome = value; OnPropertyChanged(); } }
        public string Inicial { get { return _Inicial; } set { _Inicial = value; OnPropertyChanged(); } }

        public Visibility UserInfo { get { return _UserInfo; } set { _UserInfo = value; OnPropertyChanged(); } }

        public MainWindow()
        {
            InitializeComponent();
            UserInfo = Visibility.Hidden;
            DataContext = this;
        }

        private void GridTitulo_MouseDown(object sender, MouseButtonEventArgs e)
        {

            if (e.ClickCount == 2)
            {
                if (ResizeMode != ResizeMode.CanResize &&
                    ResizeMode != ResizeMode.CanResizeWithGrip)
                {
                    return;
                }

                WindowState = WindowState == WindowState.Maximized
                    ? WindowState.Normal
                    : WindowState.Maximized;
            }
            else if (e.RightButton == MouseButtonState.Released && e.MiddleButton == MouseButtonState.Released && e.LeftButton == MouseButtonState.Pressed)
            {
                mRestoreForDragMove = WindowState == WindowState.Maximized;
                DragMove();
            }
        }

        private void GridTitulo_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (mRestoreForDragMove && Mouse.LeftButton == MouseButtonState.Pressed)
            {
                mRestoreForDragMove = false;

                var point = PointToScreen(e.MouseDevice.GetPosition(this));

                Left = point.X - (RestoreBounds.Width * 0.5);
                Top = point.Y;

                WindowState = WindowState.Normal;

                DragMove();
            }
        }

        private void GridTitulo_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            mRestoreForDragMove = false;
        }

        public async void MostrarDialogCustomizada(Object frameworkElement, string Título)
        {
            EventHandler<DialogStateChangedEventArgs> dialogManagerOnDialogOpened = null;
            dialogManagerOnDialogOpened = (o, args) =>
            {
                DialogManager.DialogOpened -= dialogManagerOnDialogOpened;
                Console.WriteLine("Custom Dialog opened!");
            };
            DialogManager.DialogOpened += dialogManagerOnDialogOpened;

            EventHandler<DialogStateChangedEventArgs> dialogManagerOnDialogClosed = null;
            dialogManagerOnDialogClosed = (o, args) =>
            {
                DialogManager.DialogClosed -= dialogManagerOnDialogClosed;
                Console.WriteLine("Custom Dialog closed!");
            };
            DialogManager.DialogClosed += dialogManagerOnDialogClosed;

            var dialog = new CustomDialog(this.MetroDialogOptions)
            {
                Content = frameworkElement,
                Title = Título,
                Foreground = Brushes.White,
                FontFamily = new FontFamily("Segoe UI Semilight"),
                Background = Application.Current.Resources["Fundo"] as SolidColorBrush
            };
            TextOptions.SetTextRenderingMode(dialog, TextRenderingMode.Aliased);

            await this.ShowMetroDialogAsync(dialog);
            await dialog.WaitUntilUnloadedAsync();
        }

        public async void MostrarMensagem(string Título, string CorpodaMensagem)
        {
            EventHandler<DialogStateChangedEventArgs> dialogManagerOnDialogOpened = null;
            dialogManagerOnDialogOpened = (o, args) =>
            {
                DialogManager.DialogOpened -= dialogManagerOnDialogOpened;
                Console.WriteLine("Custom Dialog opened!");
            };
            DialogManager.DialogOpened += dialogManagerOnDialogOpened;

            EventHandler<DialogStateChangedEventArgs> dialogManagerOnDialogClosed = null;
            dialogManagerOnDialogClosed = (o, args) =>
            {
                DialogManager.DialogClosed -= dialogManagerOnDialogClosed;
                Console.WriteLine("Custom Dialog closed!");
            };
            DialogManager.DialogClosed += dialogManagerOnDialogClosed;
            Mensagem msg = new Mensagem(CorpodaMensagem);
            var dialog = new CustomDialog(this.MetroDialogOptions)
            {
                Content = msg,
                Title = Título,
                Foreground = Brushes.White,
                FontFamily = new FontFamily("Segoe UI Semilight"),
                Background = new SolidColorBrush(Color.FromRgb(0, 118, 215))
            };
            TextOptions.SetTextRenderingMode(dialog, TextRenderingMode.Aliased);

            await this.ShowMetroDialogAsync(dialog);
            msg.BtOK.Focus();
            await dialog.WaitUntilUnloadedAsync();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            var dialog = (sender as DependencyObject).TryFindParent<BaseMetroDialog>();
            await this.HideMetroDialogAsync(dialog);
        }
    }
}
