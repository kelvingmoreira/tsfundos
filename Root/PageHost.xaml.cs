﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace TSFundos.Root
{
    /// <summary>
    /// Interação lógica para PageHost.xam
    /// </summary>
    public partial class PageHost : Page, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string contexto;
        private string _paginaanteriorxaml;

        public string Contexto
        { get { return contexto; } set { contexto = value; OnPropertyChanged("Contexto"); } }
        public string paginaanteriorxaml
        { get { return _paginaanteriorxaml; } set { _paginaanteriorxaml = value; OnPropertyChanged("paginaanteriorxaml"); } }

        public string PagePath;
        public PageHost(string caminho) 
        {
            paginaanteriorxaml = caminho;
            InitializeComponent();
            DataContext = this;
        }

        private void BtVoltar_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mw = Application.Current.MainWindow as MainWindow;
            mw.FrConteúdo.GoBack();
        }
    }
}
