﻿using FluentDesign.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TSFundos.Root.Cadastro
{
    /// <summary>
    /// Interação lógica para Menu.xam
    /// </summary>
    public partial class Menu : Page
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void NavigationView_ItemClick(object sender, FluentDesign.Controls.ItemClickEventArgs args)
        {
            (sender as NavigationView).Content = args.ClickedItem;
        }

        private void NavigationView_BackButtonClick(object sender, RoutedEventArgs e)
        {
            MainWindow mw = Application.Current.MainWindow as MainWindow;
            NavigationService.GoBack();
        }
    }
}
