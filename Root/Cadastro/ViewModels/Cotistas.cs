﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Gestora.Dados;
using Gestora.Dados.Fundos;
using Gestora.Dados.Fundos.Passivo;
using Gestora.Fundos.Passivo;
using MahApps.Metro.Controls.Dialogs;
using Gestora.Dados.Primitivos;
using TSFundos.Dialogs;
using System.Collections.Generic;
using TSFundos.Helpers;
using Microsoft.Win32;
using System.IO;
using System.Text;

namespace TSFundos.Root.Cadastro.ViewModels
{
    public class Cotistas : CotistasProvider
    {
        Dialogs.DialogManager dialogManager;

        public Cotistas()
        {
            dialogManager = new Dialogs.DialogManager();
            LoadedCommand = new RelayCommand(Loaded);
            AdicionarCommand = new RelayCommand(Adicionar);
            DetalhesCommand = new RelayCommand(Detalhes);
            AlterarCommand = new RelayCommand(Alterar);
            RemoverCommand = new RelayCommand(Remover);
        }

        #region Comandos

        public ICommand LoadedCommand { get; set; }
        public ICommand AdicionarCommand { get; set; }
        public ICommand DetalhesCommand { get; set; }
        public ICommand AlterarCommand { get; set; }
        public ICommand RemoverCommand { get; set; }


        private async void Loaded(object obj)
        {
            await RunSearchAsync();
        }

        private async void Adicionar(object obj)
        {
            var CotistaDialog = new Cadastro.CotistaDialog();
            var CotistaDialogVM = new CotistaDialog();

            MessageDialogResult result = await dialogManager.ShowDialogAsync(CotistaDialogVM, CotistaDialog);
            if (result == MessageDialogResult.Affirmative)
            {
                ContasBancárias = new ContasBancáriasProvider();
                await RunSearchAsync();
                await dialogManager.ShowMessageBox("Aviso", "Cotista cadastrado com sucesso.");
            }
        }

        private async void Detalhes(object obj)
        {
            var CotistaDialog = new Cadastro.CotistaDialog();
            var CotistaDialogVM = new CotistaDialog((Cotista)obj);
            CotistaDialogVM.IsReadOnly = true;
            CotistaDialogVM.IsEditing = true;

            MessageDialogResult result = await dialogManager.ShowDialogAsync(CotistaDialogVM, CotistaDialog);
        }

        private async void Alterar(object obj)
        {
            var CotistaDialog = new Cadastro.CotistaDialog();
            var CotistaDialogVM = new CotistaDialog((Cotista)obj);
            CotistaDialogVM.IsReadOnly = false;
            CotistaDialogVM.IsEditing = true;

            MessageDialogResult result = await dialogManager.ShowDialogAsync(CotistaDialogVM, CotistaDialog);
            if (result == MessageDialogResult.Affirmative)
            {
                await ContasBancárias.RunSearchAsync();
                await RunSearchAsync();
                await dialogManager.ShowMessageBox("Aviso", "Cotista alterado com sucesso.");
            }
        }

        private async void Remover(object obj)
        {
            MessageDialogResult result = await dialogManager.ShowDefaultDialog("Aviso", "Esta ação não pode ser desfeita. Deseja continuar?");
            if (result == MessageDialogResult.Affirmative)
            {
                Cotista cotista = obj as Cotista;
                ContasBancáriasServicer contaBancária = new ContasBancáriasServicer { ManipulationType = ManipulationType.Update };
                if (cotista.TipoPessoa != TipoPessoa.ContaeOrdem)
                {
                    contaBancária.ID = cotista.ContaBancária?.ID;
                    contaBancária.Banco = cotista.ContaBancária?.Banco;
                    contaBancária.Agência = cotista.ContaBancária?.Agência;
                    contaBancária.Conta = cotista.ContaBancária?.Conta;
                    contaBancária.Dígito = cotista.ContaBancária?.Dígito;
                }
                CotistasServicer cotistasServicer = new CotistasServicer 
                {   
                    ManipulationType = ManipulationType.Delete,
                    TipoPessoa = cotista.TipoPessoa,
                    ID = cotista.ID,
                    ContaBancária = contaBancária,
                    Cpf = cotista.Cpf,
                    Cnpj = cotista.Cnpj,
                    CódigoCotista = new Converters.NumberConverter().Convert(cotista.Nome),
                    NomeIdentificador = cotista.NomeIdentificador,
                    Distribuidor = cotista.Distribuidor,
                    Nome = cotista.Nome
                };
                cotistasServicer.ExecuteNonQuery();
                await RunSearchAsync();
            }
        }

        #endregion
    }
}
