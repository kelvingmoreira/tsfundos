﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Gestora.Dados.Fundos.Passivo;
using Gestora.Fundos.Passivo;
using TSFundos.Controls;
using MahApps.Metro.Controls.Dialogs;


namespace TSFundos.Root.Cadastro
{
    /// <summary>
    /// Interação lógica para CotistaDialog.xam
    /// </summary>
    public partial class CotistaDialog : CustomDialog
    {
        public CotistaDialog()
        {
            InitializeComponent();
            UpdateLayout();
        }
    }
}
