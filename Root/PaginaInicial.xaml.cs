﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Effects;
using Microsoft.Xaml.Behaviors;

namespace TSFundos.Root
{
    /// <summary>
    /// Interação lógica para PaginaInicial.xam
    /// </summary>
    public partial class PaginaInicial : Page
    {
        public PaginaInicial()
        {
            InitializeComponent();
            TxLocalizar.Focus();
        }



        private void BtCadastro_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Root/Cadastro/Menu.xaml", UriKind.Relative));
        }

        private void BtPassivo_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Root/Passivo/Menu.xaml", UriKind.Relative));
        }

        private void BtValidação_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("Root/Validação/Menu.xaml", UriKind.Relative));
        }
    }
}
