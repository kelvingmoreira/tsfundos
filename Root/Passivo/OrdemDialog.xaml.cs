﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Gestora.Dados.Fundos.Passivo;
using Gestora.Fundos.Passivo;
using TSFundos.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace TSFundos.Root.Passivo
{
    /// <summary>
    /// Interação lógica para OrdemManual.xam
    /// </summary>
    public partial class OrdemDialog : CustomDialog
    {
        public OrdemDialog()
        {
            InitializeComponent();
            CbFundo.Focus();
        }

        public OrdemDialog(Cotista cotista)
        {
            InitializeComponent();

            CotistasProvider cotistas = new CotistasProvider(cotista.Nome);
            ppCotista.ItemsSource = cotistas.Collection;
            ppCotista.CbQuery.Visibility = Visibility.Visible;
            ppCotista.TxPerson.Visibility = Visibility.Collapsed;
            ppCotista.SelectedItem = cotistas.Collection.Single(x => x.Nome == cotista.Nome);
            CbFundo.Focus();
        }

        private void PersonPicker_ClickSearch(object sender, RoutedEventArgs e)
        {

            PersonPicker pp = sender as PersonPicker;
            if (pp.TxPerson.Text == string.Empty)
            {
                pp.Resultado = "A busca é pouco específica.";
                pp.ItemsSource = null;
                return;
            }
            CotistasProvider cotistas = new CotistasProvider(pp.TxPerson.Text);
            if (cotistas.Collection.Count > 20)
            {
                pp.Resultado = "A busca é pouco específica.";
                pp.ItemsSource = null;
                return;
            }
            if (cotistas.Collection.Count == 0) pp.Resultado = "A busca não obteve resultados.";
            pp.ItemsSource = cotistas.Collection;
            pp.Resultado = cotistas.Collection.Count == 0 ? 
                           "A busca não obteve resultados." :
                           string.Format($"A busca obteve {cotistas.Collection.Count} resultado{(cotistas.Collection.Count == 1 ? "" : "s")}.");
        }

        private async void BtBoletar_Click(object sender, RoutedEventArgs e)
        {
            await Task.Delay(200);
            //BtConfirmar.Focus();
        }

        private async void MyPopup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                await Task.Delay(200);
                ((System.Windows.Controls.Primitives.Popup)sender).IsOpen = false;
            }
        }
    }
}
