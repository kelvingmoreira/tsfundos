﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Gestora.Dados;
using Gestora.Dados.Fundos;
using Gestora.Dados.Fundos.Passivo;
using Gestora.Fundos.Passivo;
using MahApps.Metro.Controls.Dialogs;
using TSFundos.Dialogs;
using System.Collections.Generic;
using TSFundos.Helpers;
using Microsoft.Win32;
using System.IO;
using System.Text;

namespace TSFundos.Root.Passivo.ViewModels
{
    public class Ordens : BoletasProvider
    {
        private string buscavazia;
        public string BuscaVazia { get { return buscavazia; } set { buscavazia = value; OnPropertyChanged(); } }

        private bool? allselected;
        public bool? AllSelected
        {
            get 
            {
                if (Collection == null || Collection.Count == 0) return false;
                bool containstrue = false, containsfalse = false;
                foreach (Boleta b in Collection)
                {
                    if (b.IsSelected) containstrue = true;
                    if (!b.IsSelected) containsfalse = true;
                }
                if (containstrue && containsfalse) return null;
                else if (containstrue) return true;
                else return false;
            }
            set
            {
                allselected = value;
                if (Collection != null) Collection.ToList().ForEach(x => x.IsSelected = value.Value);
                OnPropertyChanged();
            }
        }

        Dialogs.DialogManager dialogManager;

        public Ordens()
        {
            AdicionarCommand = new RelayCommand(Adicionar);
            ConsultarCommand = new RelayCommand(Consultar);
            CheckBoxCommand = new RelayCommand(CheckChanged);
            EditarCommand = new RelayCommand(Editar, Editar_CanExecute);
            VerBoletaCommand = new RelayCommand(VerBoleta, VerBoleta_CanExecute);
            AprovarCommand = new RelayCommand(Aprovar, GroupSelection_CanExecute);
            ReprovarCommand = new RelayCommand(Reprovar, GroupSelection_CanExecute);
            ExportarCommand = new RelayCommand(Exportar, Exportar_CanExecute);
            dialogManager = new Dialogs.DialogManager();
        }

        #region Comandos

        public ICommand AdicionarCommand { get; set; }
        public ICommand CheckBoxCommand { get; set; }
        public ICommand ConsultarCommand { get; set; }
        public ICommand EditarCommand { get; set; }
        public ICommand VerBoletaCommand { get; set; }
        public ICommand AprovarCommand { get; set; }
        public ICommand ReprovarCommand { get; set; }
        public ICommand ExportarCommand { get; set; }

        private async void Adicionar(object obj)
        {
            var OrdemDialog = new Passivo.OrdemDialog();
            var OrdemDialogVM = new OrdemDialog();

            MessageDialogResult result = await dialogManager.ShowDialogAsync(OrdemDialogVM, OrdemDialog);
            if (result == MessageDialogResult.Affirmative) await dialogManager.ShowMessageBox("Aviso", "Operação boletada com sucesso.");
        }

        private void CheckChanged(object obj)
        {
            OnPropertyChanged(nameof(AllSelected));
        }

        private async void Consultar(object obj)
        {
            Validate();
            if (ValidationErrors.Count == 0)
            {
                await RunSearchAsync();
                if (Collection == null || Collection.Count == 0)
                {
                    BuscaVazia = "A busca não obteve resultados.";
                }
                else BuscaVazia = null;
                OnPropertyChanged(nameof(AllSelected));
            }
        }

        private async void Editar(object obj)
        {
            Boleta boleta = obj as Boleta;
            var OrdemDialog = new Passivo.OrdemDialog(boleta.Cotista);
            var OrdemDialogVM = new OrdemDialog((Boleta)obj, Fundos);
            OrdemDialogVM.IsEditing = true;

            
            MessageDialogResult result = await dialogManager.ShowDialogAsync(OrdemDialogVM, OrdemDialog);
            if (result == MessageDialogResult.Affirmative)
            {
                await RunSearchAsync();
                await dialogManager.ShowMessageBox("Aviso", "Boleta alterada com sucesso.");
                OnPropertyChanged(nameof(AllSelected));
            }
        }

        private bool Editar_CanExecute(object obj)
        {
            if (obj == null) return false;
            if (((Boleta)obj).Status == Status.Reprovado || ((Boleta)obj).Status == Status.EmEspera) return true;
            else return false;
        }

        private async void VerBoleta(object obj)
        {
            var OrdemDialog = new Passivo.OrdemDialog(((Boleta)obj).Cotista);
            var OrdemDialogVM = new OrdemDialog((Boleta)obj, Fundos);
            OrdemDialogVM.IsReadOnly = true;

            MessageDialogResult result = await dialogManager.ShowDialogAsync(OrdemDialogVM, OrdemDialog);
        }

        private bool VerBoleta_CanExecute(object obj)
        {
            if (obj == null) return false;
            else return true;
        }

        private async void Aprovar(object obj)
        {
            IEnumerable<Boleta> boletasSelecionadas = (obj as ObservableCollection<Boleta>).Where(x => x.IsSelected == true);
            foreach(Boleta boleta in boletasSelecionadas)
            {
                BoletasServicer boletasServicer = new BoletasServicer
                {
                    ManipulationType = ManipulationType.Update,
                    ID = boleta.ID,
                    Status = Status.Aprovado,
                    Comentários = boleta.Comentários,
                    Cotista = boleta.Cotista,
                    DataAgendada = boleta.DataAgendada,
                    DataRegistro = boleta.DataRegistro,
                    FormaImportação = boleta.FormaImportação,
                    Fundo = boleta.Fundo,
                    FormaLiquidação = boleta.FormaLiquidação,
                    Operação = boleta.Operação,
                    TipoValor = boleta.TipoValor,
                    Valor = boleta.TipoValor == TipoValor.EmCotas ? boleta.Cotas : boleta.ValorFinanceiro

                };
                if (boletasServicer.Status != Status.Validado && boletasServicer.Operação == Gestora.Fundos.Passivo.Operação.ResgateTotal) boletasServicer.Valor = null;
                boletasServicer.ExecuteNonQuery();
            }
            await RunSearchAsync();
            OnPropertyChanged(nameof(AllSelected));
        }

        private async void Reprovar(object obj)
        {
            IEnumerable<Boleta> boletasSelecionadas = (obj as ObservableCollection<Boleta>).Where(x => x.IsSelected == true);
            foreach (Boleta boleta in boletasSelecionadas)
            {
                BoletasServicer boletasServicer = new BoletasServicer
                {
                    ManipulationType = ManipulationType.Update,
                    ID = boleta.ID,
                    Status = Status.Reprovado,
                    Comentários = boleta.Comentários,
                    Cotista = boleta.Cotista,
                    DataAgendada = boleta.DataAgendada,
                    DataRegistro = boleta.DataRegistro,
                    FormaImportação = boleta.FormaImportação,
                    Fundo = boleta.Fundo,
                    FormaLiquidação = boleta.FormaLiquidação,
                    Operação = boleta.Operação,
                    TipoValor = boleta.TipoValor,
                    Valor = boleta.TipoValor == TipoValor.EmCotas ? boleta.Cotas : boleta.ValorFinanceiro
                };
                if (boletasServicer.Status != Status.Validado && boletasServicer.Operação == Gestora.Fundos.Passivo.Operação.ResgateTotal) boletasServicer.Valor = null;
                boletasServicer.ExecuteNonQuery();
            }
            await RunSearchAsync();
            OnPropertyChanged(nameof(AllSelected));
        }

        private bool GroupSelection_CanExecute(object obj)
        {
            if (AllSelected == true || AllSelected == null)
            {
                IEnumerable<Boleta> boletasSelecionadas = (obj as ObservableCollection<Boleta>).Where(x => x.IsSelected == true && x.Status == Status.Validado);
                if (boletasSelecionadas.Count() == 0) return true;
                else return false;
            }
            else return false;
        }


        private async void Exportar(object obj)
        {
            IEnumerable<Boleta> inválidos = Collection
                .Where
                (
                    x => (x.Cotista.TipoPessoa == TipoPessoa.ContaeOrdem && x.Cotista.Distribuidor.ContaAplicação == null && x.Operação == Gestora.Fundos.Passivo.Operação.Aplicação) ||
                         (x.Cotista.TipoPessoa == TipoPessoa.ContaeOrdem && x.Cotista.Distribuidor.ContaResgate == null && x.Operação != Gestora.Fundos.Passivo.Operação.Aplicação) ||
                         (x.Cotista.TipoPessoa != TipoPessoa.ContaeOrdem && x.Cotista.ContaBancária == null)
                );
            if (inválidos.Count() != 0)
            {
                string cotistas = "";
                foreach (Boleta b in inválidos) cotistas += b.Cotista.Nome + "\n";
                await dialogManager.ShowMessageBox("Erro", $"O{(inválidos.Count() == 1 ? "" : "s")} cotista{(inválidos.Count() == 1 ? "" : "s")} abaixo não possu{(inválidos.Count() == 1 ? "i" : "em")} " +
                    $"dados bancários:\n\n{cotistas}\ncorrija o erro e tente novamente.");
                return;
            }
            string FileName = $"Movimentações - {Fundo.NomeFantasia.ReplaceInvalidChars()}{(Distribuidor == null ? " " : " - " + Distribuidor.RazãoSocial.ReplaceInvalidChars())} {DateTime.Today:dd-MM-yyyy}";
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = FileName; 
            dlg.DefaultExt = ".csv"; 
            dlg.Filter = "CSV documents (.csv)|*.csv";

            bool? result = dlg.ShowDialog();

            if (result == true)
            {
                string filename = dlg.FileName;
                IEnumerable<string> list = BoletaToCsv(Collection);

                using (var textWriter = new StreamWriter(filename, false, Encoding.UTF8))
                {
                    foreach (var line in list)
                    {
                        textWriter.WriteLine(line);
                    }
                }

                await dialogManager.ShowMessageBox("Aviso", "Arquivo gerado com sucesso.");
            }
        }

        private bool Exportar_CanExecute(object obj)
        {
            if (AllSelected == true || AllSelected == null) return true;
            else return false;
        }

        public static IEnumerable<string> BoletaToCsv(ObservableCollection<Boleta> boletas)
        {
            yield return string.Join(";", "Agendamento", "Cotização", "Liquidação", "Distribuidor", "Cpf/Cnpj", "Cotista", "Operação", "Fundo", "Cnpj", "Valor Financeiro", "Cotas",
                "Liquidação", "Banco", "Agência", "Conta", "Dígito");

            foreach (Boleta boleta in boletas)
            {
                if (boleta.IsSelected == false) continue;
                if (boleta.Cotista.TipoPessoa != TipoPessoa.ContaeOrdem)
                {
                    if (boleta.Operação == Gestora.Fundos.Passivo.Operação.Aplicação)
                        yield return string.Join(";", boleta.DataAgendada.ToString("dd-MM-yyyy"), boleta.DataCotização.ToString("dd-MM-yyyy"), boleta.DataLiquidação.ToString("dd-MM-yyyy"), "", boleta.Cotista.TipoPessoa == TipoPessoa.Jurídica ? boleta.Cotista.Cnpj : boleta.Cotista.Cpf,
                        boleta.Cotista, boleta.Operação.GetEnumDescription(), boleta.Fundo, boleta.Fundo.Código, boleta.ValorFinanceiro.ToString(), boleta.Status == Status.Validado ? boleta.Cotas.ToString() : "",
                        boleta.FormaLiquidação.GetEnumDescription(), boleta.Cotista.ContaBancária?.Banco, boleta.Cotista.ContaBancária?.Agência,
                        boleta.Cotista.ContaBancária?.Conta, boleta.Cotista.ContaBancária?.Dígito);
                    else if (boleta.Operação == Gestora.Fundos.Passivo.Operação.ResgateTotal)
                        yield return string.Join(";", boleta.DataAgendada.ToString("dd-MM-yyyy"), boleta.DataCotização.ToString("dd-MM-yyyy"), boleta.DataLiquidação.ToString("dd-MM-yyyy"), "", boleta.Cotista.TipoPessoa == TipoPessoa.Jurídica ? boleta.Cotista.Cnpj : boleta.Cotista.Cpf,
                        boleta.Cotista, boleta.Operação.GetEnumDescription(), boleta.Fundo, boleta.Fundo.Código, boleta.ValorFinanceiro.ToString(),
                        boleta.Cotas.ToString(), boleta.FormaLiquidação.GetEnumDescription(), boleta.Cotista.ContaBancária?.Banco,
                        boleta.Cotista.ContaBancária?.Agência, boleta.Cotista.ContaBancária?.Conta, boleta.Cotista.ContaBancária?.Dígito);
                    else yield return string.Join(";", boleta.DataAgendada.ToString("dd-MM-yyyy"), boleta.DataCotização.ToString("dd-MM-yyyy"), boleta.DataLiquidação.ToString("dd-MM-yyyy"), "", boleta.Cotista.TipoPessoa == TipoPessoa.Jurídica ? boleta.Cotista.Cnpj : boleta.Cotista.Cpf,
                        boleta.Cotista, boleta.Operação.GetEnumDescription(), boleta.Fundo, boleta.Fundo.Código, boleta.ValorFinanceiro.ToString(), boleta.Cotas.ToString(),
                        boleta.FormaLiquidação.GetEnumDescription(), boleta.Cotista.ContaBancária?.Banco, boleta.Cotista.ContaBancária?.Agência,
                        boleta.Cotista.ContaBancária?.Conta, boleta.Cotista.ContaBancária?.Dígito);
                }
                else
                {
                    if (boleta.Operação == Gestora.Fundos.Passivo.Operação.Aplicação)
                        yield return string.Join(";", boleta.DataAgendada.ToString("dd-MM-yyyy"), boleta.DataCotização.ToString("dd-MM-yyyy"), boleta.DataLiquidação.ToString("dd-MM-yyyy"), boleta.Cotista.Distribuidor, boleta.Cotista.Distribuidor.Código,
                        boleta.Cotista, boleta.Operação.GetEnumDescription(), boleta.Fundo, boleta.Fundo.Código, boleta.ValorFinanceiro.ToString(), boleta.Status == Status.Validado ? boleta.Cotas.ToString() : "",
                        boleta.FormaLiquidação.GetEnumDescription(), boleta.Cotista.Distribuidor.ContaAplicação?.Banco, boleta.Cotista.Distribuidor.ContaAplicação?.Agência,
                        boleta.Cotista.Distribuidor.ContaAplicação?.Conta, boleta.Cotista.Distribuidor?.ContaAplicação.Dígito);
                    else if (boleta.Operação == Gestora.Fundos.Passivo.Operação.ResgateTotal)
                        yield return string.Join(";", boleta.DataAgendada.ToString("dd-MM-yyyy"), boleta.DataCotização.ToString("dd-MM-yyyy"), boleta.DataLiquidação.ToString("dd-MM-yyyy"), boleta.Cotista.Distribuidor, boleta.Cotista.Distribuidor.Código,
                        boleta.Cotista, boleta.Operação.GetEnumDescription(), boleta.Fundo, boleta.Fundo.Código, boleta.ValorFinanceiro.ToString(),
                        boleta.Cotas.ToString(), boleta.FormaLiquidação.GetEnumDescription(), boleta.Cotista.Distribuidor.ContaResgate.Banco,
                        boleta.Cotista.Distribuidor.ContaResgate.Agência, boleta.Cotista.Distribuidor.ContaResgate.Conta, boleta.Cotista.Distribuidor.ContaResgate.Dígito);
                    else yield return string.Join(";", boleta.DataAgendada.ToString("dd-MM-yyyy"), boleta.DataCotização.ToString("dd-MM-yyyy"), boleta.DataLiquidação.ToString("dd-MM-yyyy"), boleta.Cotista.Distribuidor, boleta.Cotista.Distribuidor.Código,
                        boleta.Cotista, boleta.Operação.GetEnumDescription(), boleta.Fundo, boleta.Fundo.Código, boleta.ValorFinanceiro.ToString(), boleta.Cotas.ToString(),
                        boleta.FormaLiquidação.GetEnumDescription(), boleta.Cotista.Distribuidor.ContaResgate.Banco, boleta.Cotista.Distribuidor.ContaResgate.Agência,
                        boleta.Cotista.Distribuidor.ContaResgate.Conta, boleta.Cotista.Distribuidor.ContaResgate.Dígito);
                }
            }
        }

        #endregion
    }
}
