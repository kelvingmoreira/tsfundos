﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gestora.Dados;
using Gestora.Dados.Fundos;
using Gestora.Dados.Fundos.Passivo;
using MahApps.Metro.Controls.Dialogs;
using TSFundos.Dialogs;
using System.Windows.Input;
using System.ComponentModel;
using Gestora.Helpers;
using Gestora.Fundos.Passivo;
using Gestora.Fundos;

namespace TSFundos.Root.Passivo.ViewModels
{
    public class OrdemDialog : BoletasServicer, IDialogViewModel<MessageDialogResult>
    {
        #region IDialogViewModel members
        private readonly TaskCompletionSource<MessageDialogResult> _tcs;
        public void Close(MessageDialogResult result)
        {
            _tcs.SetResult(result);
            Closed?.Invoke(this, EventArgs.Empty);
        }
        public Task<MessageDialogResult> Task => _tcs.Task;
        public event EventHandler Closed;
        private ICommand _dialogCommand;
        public ICommand DialogCommand { get { return _dialogCommand; } }
        private void OnDialogCommandExecute(MessageDialogResult messageDialogResult)
        {
            if (messageDialogResult == MessageDialogResult.Affirmative)
            {
                Validate();
                if (ValidationErrors.Count == 0)
                {
                    ExecuteNonQuery();
                    Close(messageDialogResult);
                }
            }
            else if (messageDialogResult == MessageDialogResult.Negative)
            {
                Close(messageDialogResult);
            }
        }
        #endregion

        private bool isreadonly;
        private bool isediting;
        public bool IsReadOnly { get { return isreadonly; } set { isreadonly = value; OnPropertyChanged(null); } }
        public bool IsEnabled { get { return !IsReadOnly; } }
        public bool IsEditing { get { return isediting; } set { isediting = value; OnPropertyChanged(); } }
        public string PrimaryButtonContent { get { return IsEditing ? "Alterar" : "Boletar"; } }
        public string NegativeButtonContent { get { return IsReadOnly ? "Fechar" : "Cancelar"; } }

        public OrdemDialog()
        {
            Fundos = new Fundos().Collection;
            _tcs = new TaskCompletionSource<MessageDialogResult>();
            _dialogCommand = new RelayCommand<MessageDialogResult>(OnDialogCommandExecute, result => true);

            FormaImportação = FormaImportação.Manual;
        }
        public OrdemDialog(Boleta boleta, ObservableCollection<Fundo> fundos)
        {
            Fundos = fundos;
            _tcs = new TaskCompletionSource<MessageDialogResult>();
            _dialogCommand = new RelayCommand<MessageDialogResult>(OnDialogCommandExecute, result => true);

            ManipulationType = Gestora.Dados.ManipulationType.Update;
            ID = boleta.ID;
            Fundo = Fundos.Single(x => x.Código == boleta.Fundo.Código);
            DataAgendada = boleta.DataAgendada;
            Operação = boleta.Operação;
            if (boleta.Status != Gestora.Fundos.Passivo.Status.Validado && boleta.Operação == Gestora.Fundos.Passivo.Operação.ResgateTotal) Valor = null; 
            else Valor = boleta.TipoValor == TipoValor.EmCotas ? Math.Abs(boleta.Cotas.Value) : Math.Abs(boleta.ValorFinanceiro.Value);
            Cotista = boleta.Cotista;
            FormaLiquidação = boleta.FormaLiquidação;
            DataRegistro = boleta.DataRegistro;
            Comentários = boleta.Comentários;
            TipoValor = boleta.TipoValor;
            Status = boleta.Status;
        }
    }
}
