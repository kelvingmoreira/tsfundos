﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gestora.Dados;
using Gestora.Dados.Fundos;
using Gestora.Dados.Fundos.Passivo;
using MahApps.Metro.Controls.Dialogs;
using TSFundos.Dialogs;
using System.Windows.Input;
using System.ComponentModel;
using Gestora.Helpers;
using Gestora.Fundos.Passivo;
using TSFundos.Helpers;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using Microsoft.Win32;
using System.Windows;
using System.Reflection;

namespace TSFundos.Root.Passivo.ViewModels
{
    public class ExtratoPosição : PosiçõesProvider
    {
        private string buscavazia;
        public string BuscaVazia { get { return buscavazia; } set { buscavazia = value; OnPropertyChanged(); } }

        Dialogs.DialogManager dialogManager;

        public ExtratoPosição()
        {
            ConsultarCommand = new RelayCommand(Consultar);
            ExportarCommand = new RelayCommand(Exportar);
            dialogManager = new Dialogs.DialogManager();
        }

        #region Comandos

        public ICommand ConsultarCommand { get; set; }
        public ICommand ExportarCommand { get; set; }

        private async void Consultar(object obj)
        {
            Validate();
            if (ValidationErrors.Count == 0)
            {
                await RunSearchAsync();
                if (Collection == null || Collection.Count == 0)
                {
                    BuscaVazia = "A busca não obteve resultados.";
                }
                else BuscaVazia = null;
            }
        }

        private async void Exportar(object obj)
        {
            Validate();
            if (ValidationErrors.Count == 0)
            {
                RunSearch();
                if (Collection == null || Collection.Count == 0)
                {
                    await dialogManager.ShowMessageBox("Aviso", "A busca não obteve resultados.");
                    return;
                }
            }
            else return;

            string FileName = $"Extrato de Posição - {Fundo.NomeFantasia.ReplaceInvalidChars()}{(Distribuidor == null ? " " : " - " + Distribuidor.RazãoSocial.ReplaceInvalidChars())}{Posição.Value:dd-MM-yyyy}";
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = FileName; // Default file name
            dlg.DefaultExt = ".csv"; // Default file extension
            dlg.Filter = "CSV documents (.csv)|*.csv"; // Filter files by extension

            // Show save file dialog box
            bool? result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;
                IEnumerable<string> list = Collection.ToCsv();
                using (var textWriter = new StreamWriter(filename, false, Encoding.UTF8))
                {
                    foreach (var line in list)
                    {
                        textWriter.WriteLine(line);
                    }
                }

                await dialogManager.ShowMessageBox("Aviso", "Arquivo gerado com sucesso.");
            }
        }

        #endregion
    }
}
