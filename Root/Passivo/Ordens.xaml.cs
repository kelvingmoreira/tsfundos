﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Gestora.Dados.Fundos.Passivo;
using Gestora.Fundos.Passivo;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace TSFundos.Root.Passivo
{
    using TSFundos.Dialogs;
    /// <summary>
    /// Interação lógica para Ordens.xam
    /// </summary>
    public partial class Ordens : UserControl
    {
        public Ordens()
        {
            InitializeComponent();
        }

        private void HlConsultar_Click(object sender, RoutedEventArgs e)
        {
            PageHost pg = new PageHost("../Root/Passivo/OrdensConsulta.xaml");
            pg.Contexto = "Emitir/rever ordens";
            MainWindow mw = Application.Current.Windows[0] as MainWindow;
            mw.FrConteúdo.Navigate(pg);
        }
    }
}
