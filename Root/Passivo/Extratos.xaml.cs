﻿using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TSFundos.Root.Passivo
{
    using TSFundos.Dialogs;
    /// <summary>
    /// Interação lógica para ExtratoPosição.xaml
    /// </summary>
    public partial class Extratos : UserControl
    {
        public Extratos()
        {
            InitializeComponent();
        }

        private void HlConsulta_Click(object sender, RoutedEventArgs e)
        {
            PageHost pg = new PageHost("../Root/Passivo/ExtratoPosição.xaml");
            pg.Contexto = "Extrato de posição";
            MainWindow mw = Application.Current.Windows[0] as MainWindow;
            mw.FrConteúdo.Navigate(pg);
        }
    }
}
