﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TSFundos.Controls;
using Gestora.Dados.Fundos.Passivo;

namespace TSFundos.Root.Passivo
{
    /// <summary>
    /// Interação lógica para OrdensConsulta.xam
    /// </summary>
    public partial class OrdensConsulta : UserControl
    {
        public OrdensConsulta()
        {
            InitializeComponent();
        }

        private void PersonPicker_ClickSearch(object sender, RoutedEventArgs e)
        {

            PersonPicker pp = sender as PersonPicker;
            if (pp.TxPerson.Text == string.Empty)
            {
                pp.Resultado = "A busca é pouco específica.";
                pp.ItemsSource = null;
                return;
            }
            CotistasProvider cotistas = new CotistasProvider(pp.TxPerson.Text);
            if (cotistas.Collection.Count > 20)
            {
                pp.Resultado = "A busca é pouco específica.";
                pp.ItemsSource = null;
                return;
            }
            if (cotistas.Collection.Count == 0) pp.Resultado = "A busca não obteve resultados.";
            pp.ItemsSource = cotistas.Collection;
            pp.Resultado = string.Format("A busca obteve {0} resultado(s).", cotistas.Collection.Count);
        }
    }
}
