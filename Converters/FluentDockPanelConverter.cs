﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MahApps.Metro.Controls;

namespace TSFundos.Converters
{
    class FluentPageConverter : IMultiValueConverter, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        
        private double _FontSize = 14;
        private Thickness _Thickness = new Thickness(0,0,0,15);
        private Thickness _Light = new Thickness(0, 6, 0, 15);
        private Thickness _Heavy = new Thickness(0, 50, 0, 15);
        public double FontSize { get { return _FontSize; } set { _FontSize = value; OnPropertyChanged(); } }
        public Thickness Thickness { get { return _Thickness; } set { _Thickness = value; OnPropertyChanged(); } }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            FontSize = 14;
            Thickness = _Light;
            DockPanel dp = (DockPanel)values[0];
            MetroWindow mw = Application.Current.MainWindow as MetroWindow;
            try
            {
                StackPanel sp1 = (StackPanel)dp.Children[0];
                StackPanel sp2 = (StackPanel)dp.Children[1];
                if (mw.ActualWidth < Int32.Parse((string)parameter))
                {
                    FontSize = 20;
                    Thickness = _Heavy;
                    DockPanel.SetDock(sp1, Dock.Bottom);
                    sp1.HorizontalAlignment = HorizontalAlignment.Stretch;
                    sp1.Width = Double.NaN;
                }
                else
                {
                    DockPanel.SetDock(sp1, Dock.Right);
                    sp1.HorizontalAlignment = HorizontalAlignment.Right;
                    sp1.Width = 250;
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
