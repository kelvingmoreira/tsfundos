﻿using Gestora.Fundos.Passivo;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Globalization;

namespace TSFundos.Converters
{
    class PosiçãoGroupTotalsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ReadOnlyObservableCollection<object>)
            {
                var items = (ReadOnlyObservableCollection<object>)value;
                double total = 0;
                foreach (Posição p in items)
                {
                    switch (parameter.ToString())
                    {
                        case nameof(p.Cotas):
                            total += p.Cotas;
                            break;
                        case nameof(p.ValorAplicação):
                            total += p.ValorAplicação;
                            break;
                        case nameof(p.ValorCorrigido):
                            total += p.ValorCorrigido;
                            break;
                        case nameof(p.ProvisãoIOF):
                            total += p.ProvisãoIOF;
                            break;
                        case nameof(p.ProvisãoIR):
                            total += p.ProvisãoIR;
                            break;
                        case nameof(p.ValorResgate):
                            total += p.ValorResgate;
                            break;
                        case nameof(p.Rendimento):
                            total += p.Rendimento;
                            break;
                    }
                }
                return total;
            }
            else if (parameter.ToString() == "cotista")
            {
                return $"{value} cotista{((int)value == 1 ? "" : "s")}";
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value;
        }

    }
}
