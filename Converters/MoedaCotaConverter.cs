﻿using Gestora.Fundos.Passivo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace TSFundos.Converters
{
    public class MoedaCotaConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            TipoValor tipoValor = (TipoValor)value;
            object parameterValue = Enum.Parse(value.GetType(), parameter as string);
            if (tipoValor == (TipoValor)parameterValue) return Visibility.Visible;
            else return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
