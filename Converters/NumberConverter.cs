﻿using Gestora.Fundos.Passivo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Text.RegularExpressions;

namespace TSFundos.Converters
{
    public class NumberConverter : IValueConverter
    {
        public string Convert(object value)
        {
            return ConvertBack(value, typeof(string), null, new CultureInfo("pt-BR")).ToString();
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return value;
            string phoneNo = Regex.Match(value.ToString(), @"\d+").Value;
            return phoneNo;
        }
    }
}
